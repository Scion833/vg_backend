const { request, response } = require('express');
const jwt = require('jsonwebtoken');

const privateKey = process.env.PRIVATEKEY;

/**
 * Envia al req.data los datos del usuario y/o administrador
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns 
 */
const validateJWT = (req = request, res = response, next) => {
	const token = req.header('x-token');
	if (!token) {
		return res.status(406).json({
			msg: 'no es aceptable',
		});
	}
	try {
		const data = jwt.verify(token, privateKey);
		req.data = getPerfiles(data.perfiles);
		next();
	} catch (error) {
		res.status(401).json({
			mensaje: 'Error en el token',
			error,
		});
	}
};

const getPerfiles=(arr)=>{
	let data={}
	if(arr){
		for(let i=0;i<arr.length;i++){
			data[arr[i].perfil]={id:arr[i].id}
		}
	}
	return data;
}

module.exports = {
	validateJWT,
};
