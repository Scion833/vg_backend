const jwt = require('jsonwebtoken');

const privateKey = process.env.PRIVATEKEY;

const generateJWT = (payload = '') => {
	return new Promise((resolve, reject) => {
		jwt.sign(
			payload,
			privateKey,
			{
				expiresIn: '24h',
			},
			(err, token) => {
				if (err) {
					reject('Error al generar token');
				} else {
					resolve(token);
				}
			}
		);
	});
};

module.exports = {
	generateJWT,
};
