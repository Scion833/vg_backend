const sqlite3 = require("sqlite3");
const fs = require("fs");
const DB_NAME = "database/database.db";
var db;
var is_off_Transaction = true;
const bcrypt = require("bcrypt");

function createDB() {
	db = new sqlite3.Database(DB_NAME, (err) => {
		if (err) {
			console.error(err.message);
		}
		console.log("Base de datos creada");
	});
}

function InitDB() {
	if (existDB()) {
		ConnectDB();
	} else {
		createDB();
		ConnectDB();
	}
	//AQUI LLAMAREMOS A LOS QUERYS
	createTables();
}

function createTableUsuarioDB() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS usuario (
            id_usuario INTEGER NOT NULL PRIMARY KEY,
            nombre TEXT,
            apellido_paterno TEXT,
            apellido_materno TEXT,
            ci INTEGER,
            extencion TEXT,
            fecha_nacimiento TEXT,
            correo TEXT UNIQUE NOT NULL,
            password TEXT,
            ciudad TEXT,
            genero INTEGER,
            direccion TEXT,
            celular TEXT,
            estado_civil TEXT,
            profesion TEXT,
			status INTEGER NOT NULL DEFAULT 1 CHECK(status IN (0, 1)))`,
			(err) => {
				if (err) {
					reject("Error: La tabla usuario no a sido creada");
				} else {
					resolve("La tabla usuario a sido creada");
				}
			}
		);
	});
}

function createIndexMailUser() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE INDEX IF NOT EXISTS x_correo_usuario ON usuario(correo)`,
			(err) => {
				if (err) {
					reject(
						"Error al create Index correo del usuario: ",
						err.message
					);
				} else {
					resolve("Index correo usuario fue creado");
				}
			}
		);
	});
}

function createTableAdministradorDB() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS administrador (
	        id_admin INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          	ci_nit TEXT,
          	path_photo TEXT,
	        nombre TEXT,
	        apellido_paterno TEXT,
	        apellido_materno TEXT,
          	nro_telefono TEXT,
	        correo TEXT UNIQUE NOT NULL,
	        password TEXT,
			status INTEGER NOT NULL DEFAULT 1 CHECK(status IN (0, 1)))`,
			(err) => {
				if (err) {
					reject("Error: La tabla administrador no a sido creada");
				} else {
					resolve("La tabla administrador a sido creada");
				}
			}
		);
	});
}

function createIndexMailAdministrador() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE INDEX IF NOT EXISTS x_correo_administrador ON administrador(correo)`,
			(err) => {
				if (err) {
					reject(
						"Error al create Index correo del administrador: ",
						err.message
					);
				} else {
					resolve("Index correo administrador fue creado");
				}
			}
		);
	});
}

/**
 * Con esta funcion creamos al super administrador
 * @returns
 */
function createDefaultAdministradorIfNotExists() {
	return new Promise((resolve, reject) => {
		db.all(`SELECT * FROM administrador WHERE id_admin=1`, (err, rows) => {
			if (err) {
				reject(
					"Hubo un Error al buscar si existe el super administrador"
				);
			} else {
				let administrador = {
					id_admin: "1",
					correo: "admin",
					password: "admin",
				};
				resolve("se encontro");
				if (rows.length === 0) {
					bcrypt
						.hash(administrador.password, 10)
						.then((hash) => {
							db.run(
								`INSERT INTO administrador(id_admin,correo,password) VALUES (?,?,?)`,
								[
									administrador.id_admin,
									administrador.correo,
									hash,
								],
								(err) => {
									if (err) {
										reject(
											"Error al insertar super administrador",
											err
										);
									} else {
										resolve(
											"Se creo el superadministrador correctamente"
										);
									}
								}
							);
						})
						.catch((err) => {
							reject("Error al encriptar");
						});
				}
			}
		});
	});
}

function createTableIdentidad() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS identidad (
            id_identidad INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            sexo TEXT NOT NULL
        )`,
			(err) => {
				if (err) {
					reject("Error al crear tabla identidad: ", err);
				} else {
					resolve("Tabla Identidad fue creada");
				}
			}
		);
	});
}

function createTableUsuarioHasIdentidad() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS usuario_has_identidad (
            id_usuario INTEGER NOT NULL,
            id_identidad INTEGER NOT NULL,
            FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario),
            FOREIGN KEY(id_identidad) REFERENCES identidad(id_identidad)
        )`,
			(err) => {
				if (err) {
					reject("Error al crear tabla identidad: ", err);
				} else {
					resolve("Tabla usuario_has_identidad a sido creada");
				}
			}
		);
	});
}

function createTableDenuncia() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS denuncia (
          id_denuncia INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          tipo TEXT,
          descripcion TEXT,
          ubicacion TEXT,
          fecha TEXT,
          seguimiento INTEGER
        )`,
			(err) => {
				if (err) {
					reject("Error: La tabla denuncia no a sido creada", err);
				} else {
					resolve("La tabla denuncia a sido creada");
				}
			}
		);
	});
}

function createTableAgresor() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS agresor (
        id_agresor INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        descripcion TEXT,
        nombre TEXT,
        apellido_paterno TEXT,
        apellido_materno TEXT
      )`,
			(err) => {
				if (err) {
					reject("Error: La tabla Agresor no a sido creada");
				} else {
					resolve("La tabla Agresor a sido creada");
				}
			}
		);
	});
}

function createTableEntidadAyuda() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS entidad_ayuda (
				id_entidad INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
				nombre TEXT,
				telefono TEXT,
				direccion TEXT,
				correo TEXT,
				tipo TEXT,
				latitud TEXT,
				longitud TEXT,
				id_admin INTEGER,
				FOREIGN KEY (id_admin) REFERENCES administrador(id_admin))`,
			(err) => {
				if (err) {
					reject("Error: La tabla entidad_ayuda no a sido creada");
				} else {
					resolve("La tabla entidad_ayuda a sido creada");
				}
			}
		);
	});
}

function createTablePrueba() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS prueba (
        id_prueba INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		id_usuario INTEGER NOT NULL,
        url_file TEXT,
        descripcion TEXT,
		FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario)
      )`,
			(err) => {
				if (err) {
					reject("Error: La tabla prueba no a sido creada");
				} else {
					resolve("La tabla prueba a sido creada");
				}
			}
		);
	});
}

//PARA VICTIMA Y TESTIGO CREATETABLE y DHASV DHAST DHASP DHASU
function createTableVictima() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS victima (
            id_victima INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            descripcion TEXT,
            nombre TEXT,
            apellido_paterno TEXT,
            apellido_materno TEXT,
            fecha_nac TEXT
          )`,
			(err) => {
				if (err) {
					reject("Error: La tabla victima no a sido creada");
				} else {
					resolve("La tabla victima a sido creada");
				}
			}
		);
	});
}

function createTableTestigo() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS testigo (
            id_testigo INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            descripcion TEXT,
            nombre TEXT,
            apellido_paterno TEXT,
            apellido_materno TEXT,
            fecha_nac TEXT
          )`,
			(err) => {
				if (err) {
					reject("Error: La tabla testigo no ha sido creada");
				} else {
					resolve("La tabla testigo a sido creada");
				}
			}
		);
	});
}

function createTableDenunciaHasVictima() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS denuncia_has_victima (
                id_denuncia INTEGER NOT NULL,
                id_victima INTEGER NOT NULL,
                FOREIGN KEY(id_denuncia) REFERENCES denuncia(id_denuncia),
                FOREIGN KEY(id_victima) REFERENCES victima(id_victima)
            )`,
			(err) => {
				if (err) {
					reject("Error al crear tabla denuncia has victima: ", err);
				} else {
					resolve("Tabla denuncia_has_victima a sido creada");
				}
			}
		);
	});
}

function createTableDenunciaHasTestigo() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS denuncia_has_testigo (
                id_denuncia INTEGER NOT NULL,
                id_testigo INTEGER NOT NULL,
                FOREIGN KEY(id_denuncia) REFERENCES denuncia(id_denuncia),
                FOREIGN KEY(id_testigo) REFERENCES testigo(id_testigo)
            )`,
			(err) => {
				if (err) {
					reject("Error al crear tabla denuncia_has_testigo: ", err);
				} else {
					resolve("Tabla denuncia_has_testigo a sido creada");
				}
			}
		);
	});
}

function createTableDenunciaHasPrueba() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS denuncia_has_prueba (
                id_denuncia INTEGER NOT NULL,
                id_prueba INTEGER NOT NULL,
                FOREIGN KEY(id_denuncia) REFERENCES denuncia(id_denuncia),
                FOREIGN KEY(id_prueba) REFERENCES prueba(id_prueba)
            )`,
			(err) => {
				if (err) {
					reject("Error al crear tabla denuncia_has_prueba: ", err);
				} else {
					resolve("Tabla denuncia_has_prueba a sido creada");
				}
			}
		);
	});
}

function createTableDenunciaHasUsuario() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS denuncia_has_usuario (
                id_denuncia INTEGER NOT NULL,
                id_usuario INTEGER NOT NULL,
                es_testigo TEXT CHECK (es_testigo IN ('0', '1')),
                anonimo TEXT CHECK (anonimo IN ('0', '1')),
                FOREIGN KEY(id_denuncia) REFERENCES denuncia(id_denuncia),
                FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario)
            )`,
			(err) => {
				if (err) {
					reject("Error al crear tabla denuncia_has_usuario: ", err);
				} else {
					resolve("Tabla denuncia_has_usuario a sido creada");
				}
			}
		);
	});
}

function createTableContacto() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS contacto (
            id_contacto INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            nombre TEXT,
            apellido_paterno TEXT,
            apellido_materno TEXT,
            telefono INTEGER,
            direccion TEXT,
			parentezco TEXT,
			id_usuario INTEGER,
            FOREIGN KEY (id_usuario) REFERENCES USUARIO (id_usuario)
        )`,
			(err) => {
				if (err) {
					reject("Error al crear la tabla Contacto", err);
				} else {
					resolve("Tabla Contacto creada");
				}
			}
		);
	});
}

function createTableDenunciaHasAgresor() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS denuncia_has_agresor(
			id_denuncia INTEGER NOT NULL,
			id_agresor INTEGER NOT NULL,
			FOREIGN KEY (id_denuncia) REFERENCES denuncia(id_denuncia),
			FOREIGN KEY (id_agresor) REFERENCES agresor(id_agresor)
		)`,
			(err) => {
				if (err) {
					reject("Error al crear tabla  denuncia_has_agresor");
				} else {
					resolve("Tabla denuncia_has_agresor a sido creada");
				}
			}
		);
	});
}

//PARA ARTICULO

function createTableArticulo() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS articulo (
            id_articulo    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            titulo         TEXT,
            descripcion    TEXT,
            autor          TEXT,
            url_imagen     TEXT,
            url_articulo   TEXT,
            fecha          TEXT,
            estado         TEXT CHECK(estado IN ('0', '1')),
            id_admin INTEGER,
            FOREIGN KEY(id_admin) REFERENCES administrador(id_admin)
        )`,
			(err) => {
				if (err) {
					reject("Error: La tabla articulo no a sido creada");
				} else {
					resolve("La tabla articulo a sido creada");
				}
			}
		);
	});
}

//Tabla Orientate

function createTableOrientate() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS orientate (
            id_orientate    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            titulo         TEXT,
            descripcion    TEXT,
            autor          TEXT,
            path_multimedia     TEXT,
            url_articulo   TEXT,
            fecha          TEXT,
            estado         TEXT CHECK(estado IN ('0', '1')),
            id_admin INTEGER,
            FOREIGN KEY(id_admin) REFERENCES administrador(id_admin)
        )`,
			(err) => {
				if (err) {
					reject("Error: La tabla orientate no ha sido creada");
				} else {
					resolve("La tabla orientate ha sido creada");
				}
			}
		);
	});
}

function createTableAlerta() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS alerta (
				id_alerta INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
				latitud REAL DEFAULT 0,
				longitud REAL DEFAULT 0,
				es_verdadera INTEGER,
				date_alerta TEXT DEFAULT (date('now','localtime') ||' '|| time('now','localtime')),
				estado INTEGER DEFAULT 1,
				id_usuario INTEGER,
				FOREIGN KEY (id_usuario) REFERENCES USUARIO (id_usuario)
			)`,
			(err) => {
				if (err) {
					reject("Error al crear la tabla Alerta", err);
				} else {
					resolve("Tabla Alerta creada");
				}
			}
		);
	});
}

/*function createTableUsuario_Has_Contacto() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS usuario_has_contacto (
            id_usuario INTEGER,
            id_contacto INTEGER,
            parentezco TEXT,
            FOREIGN KEY (id_usuario) REFERENCES USUARIO (id_usuario),
            FOREIGN KEY (id_contacto) REFERENCES CONTACTO (id_contacto)
        )`,
			(err) => {
				if (err) {
					reject("Error al crear la tabla usuario_has_contacto", err);
				} else {
					resolve("Tabla usuario_has_contacto creada");
				}
			}
		);
	});
}*/

function createTableEvento() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS evento (
                id_evento INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                nombre TEXT,
				lugar TEXT,
                fecha_inicio TEXT,
                fecha_final TEXT,
                descripcion TEXT,
                horarios TEXT,
                estado TEXT
            )`,
			(err) => {
				if (err) {
					reject("Error: La tabla evento no ha sido creada", err);
				} else {
					resolve("La tabla evento a sido creada");
				}
			}
		);
	});
}

function createTableAdministradorHasEvento() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS administrador_has_evento (
                id_admin INTEGER NOT NULL,
                id_evento INTEGER NOT NULL,
                FOREIGN KEY(id_admin) REFERENCES administrador(id_admin),
                FOREIGN KEY(id_evento) REFERENCES evento(id_evento)
            )`,
			(err) => {
				if (err) {
					reject(
						"Error al crear tabla administrador_has_evento: ",
						err
					);
				} else {
					resolve("Tabla administrador_has_evento a sido creada");
				}
			}
		);
	});
}

function createTableUsuarioHasEvento() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS usuario_has_evento(
			id_usuario INTEGER NOT NULL,
			id_evento INTEGER NOT NULL,
			FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario),
			FOREIGN KEY(id_evento) REFERENCES evento(id_evento)
		)`,
			(err) => {
				if (err) {
					reject(`Error al crear tabla usuario_has_evento`, err);
				} else {
					resolve("La Tabla usuario_has_evento a sido creada");
				}
			}
		);
	});
}

//Tabla Banco Preg-Resp

function createTableBancoPreguntasRespuestas() {
	return new Promise((resolve, reject) => {
		db.run(
			`CREATE TABLE IF NOT EXISTS banco_preguntas_respuestas (
				id_bpr		   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
				pregunta TEXT,
				respuesta TEXT
	 		)`,
			(err) => {
				if (err) {
					reject("Error: La tabla de Preg-Resp no ha sido creada");
				} else {
					resolve("La tabla de Preg-Resp ha sido creada");
				}
			}
		);
	});
}

function createTables() {
	createTableUsuarioDB()
		.then((res) => {
			console.log(res);
			return createTableAdministradorDB();
		})
		.then((res) => {
			console.log(res);
			return createIndexMailUser();
		})
		.then((res) => {
			console.log(res);
			return createIndexMailAdministrador();
		})
		.then((res) => {
			console.log(res);
			return createDefaultAdministradorIfNotExists();
		})
		.then((res) => {
			console.log(res);
			return createTableIdentidad();
		})
		.then((res) => {
			console.log(res);
			return createTableUsuarioHasIdentidad();
		})
		.then((res) => {
			console.log(res);
			return createTableDenuncia();
		})
		.then((res) => {
			console.log(res);
			return createTableAgresor();
		})
		.then((res) => {
			console.log(res);
			return createTableVictima();
		})
		.then((res) => {
			console.log(res);
			return createTableTestigo();
		})

		.then((res) => {
			console.log(res);
			return createTableEntidadAyuda();
		})

		.then((res) => {
			console.log(res);
			return createTableDenunciaHasUsuario();
		})
		.then((res) => {
			console.log(res);
			return createTableDenunciaHasVictima();
		})
		.then((res) => {
			console.log(res);
			return createTableDenunciaHasTestigo();
		})
		.then((res) => {
			console.log(res);
			return createTableDenunciaHasPrueba();
		})
		.then((res) => {
			console.log(res);
			return createTablePrueba();
		})
		.then((res) => {
			console.log(res);
			return createTableEvento();
		})
		.then((res) => {
			console.log(res);
			return createTableAdministradorHasEvento();
		})
		.then((res) => {
			console.log(res);
			return createTableArticulo();
		})
		.then((res) => {
			console.log(res);
			return createTableOrientate();
		})
		.then((res) => {
			console.log(res);
			return createTableDenunciaHasAgresor();
		})
		.then((res) => {
			console.log(res);
			return createTableContacto();
		})
		.then((res) => {
			console.log(res);
			return createTableAlerta();
		})
		.then((res) => {
			console.log(res);
			return createTableUsuarioHasEvento();
		})
		/*.then((res) => {
			console.log(res);
			return createTableUsuario_Has_Contacto();
		})*/
		.then((res) => {
			console.log(res);
			return createTableBancoPreguntasRespuestas();
		})
		.then((res) => {
			console.log(res);
		})
		.catch((err) => console.log(err));
}

function ConnectDB() {
	db = new sqlite3.Database(DB_NAME, (err) => {
		if (err) {
			console.error("error al conectar", err.message);
		}
	});
	console.log("conectado a la base de datos");
}

function existDB() {
	return fs.existsSync(DB_NAME);
}

function CloseDB() {
	db.close((err) => {
		if (err) {
			console.error(err.message);
		}
		console.log("Se Cerro la conexion con la base de datos");
	});
}

function GetDB() {
	return db;
}

function IsOffTransaction() {
	return is_off_Transaction;
}

function SetTransaction(value) {
	is_off_Transaction = value;
}

module.exports = {
	InitDB,
	CloseDB,
	GetDB,
	IsOffTransaction,
	SetTransaction,
};
