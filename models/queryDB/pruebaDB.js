const { GetDB } = require("../database");

/**
 * Sirve para registrar el prueba
 * @param {JSON} prueba
 * @param {function} callback
 */
function CreatePruebaDB(prueba, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO prueba(id_usuario,url_file, descripcion)
    VALUES (?,?,?)`,
		[prueba.id_usuario, prueba.url_file, prueba.descripcion],
		(err) => {
			callback(err);
		}
	);
}

function GetPruebaByIdUsuarioAndDenunciaDB(prueba, callback) {
	let db = GetDB();
	db.all(
		`SELECT p.id_prueba,p.descripcion,p.url_file FROM prueba p
	LEFT JOIN denuncia_has_prueba dhp ON p.id_prueba=dhp.id_prueba 
	AND dhp.id_denuncia=?
	WHERE dhp.id_prueba IS NULL AND p.id_usuario=?
	LIMIT ? OFFSET ?`,
		[prueba.id_denuncia, prueba.id_usuario, prueba.cantidad, prueba.desde],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetCantidadPruebasDB(id_denuncia, id_usuario, callback) {
	let db = GetDB();
	db.all(
		`SELECT count(*) AS cantidad FROM prueba p
	LEFT JOIN denuncia_has_prueba dhp ON p.id_prueba=dhp.id_prueba 
	AND dhp.id_denuncia=?
	WHERE dhp.id_prueba IS NULL AND p.id_usuario=?`,
		[id_denuncia, id_usuario],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetPruebaAgregadaByIdUsuarioAndDenunciaDB(prueba, callback) {
	let db = GetDB();
	db.all(
		`SELECT p.id_prueba,p.descripcion,p.url_file FROM prueba p
	LEFT JOIN denuncia_has_prueba dhp ON p.id_prueba=dhp.id_prueba 
	AND dhp.id_denuncia=?
	WHERE dhp.id_prueba IS NOT NULL AND p.id_usuario=?
	LIMIT ? OFFSET ?`,
		[prueba.id_denuncia, prueba.id_usuario, prueba.cantidad, prueba.desde],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetCantidadPruebasAgregadasDB(id_denuncia, id_usuario, callback) {
	let db = GetDB();
	db.all(
		`SELECT count(*) AS cantidad FROM prueba p
	LEFT JOIN denuncia_has_prueba dhp ON p.id_prueba=dhp.id_prueba 
	AND dhp.id_denuncia=?
	WHERE dhp.id_prueba IS NOT NULL AND p.id_usuario=?`,
		[id_denuncia, id_usuario],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function AddDenunciaHasPruebaDB(id_denuncia, id_prueba, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO denuncia_has_prueba(id_denuncia,id_prueba) VALUES (?,?)`,
		[id_denuncia, id_prueba],
		(err) => {
			callback(err);
		}
	);
}

function DeleteDenunciaHasPruebaDB(id_denuncia, id_prueba, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM denuncia_has_prueba
	WHERE id_denuncia=? AND id_prueba=?`,
		[id_denuncia, id_prueba],
		(err) => {
			callback(err);
		}
	);
}

module.exports = {
	CreatePruebaDB,
	GetPruebaByIdUsuarioAndDenunciaDB,
	GetCantidadPruebasDB,
	GetPruebaAgregadaByIdUsuarioAndDenunciaDB,
	GetCantidadPruebasAgregadasDB,
	AddDenunciaHasPruebaDB,
	DeleteDenunciaHasPruebaDB,
};
