const { GetDB } = require("../database");

/**
 * Sirve para registrar testigo
 * @param {JSON} testigo
 * @param {function} callback
 */
function CreateTestigoDB(testigo, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO testigo(descripcion, nombre, apellido_paterno, apellido_materno, fecha_nac)
    VALUES (?, ?, ?, ?, ?)`,
		[
            testigo.descripcion,
			testigo.nombre,
			testigo.apellido_paterno,
			testigo.apellido_materno,
			testigo.fecha_nac,
		],
		function (err) {
			if (err) {
				callback(err);
				return;
			}
			const id_testigo = this.lastID;
			if (testigo.id_denuncia !== 0) {
				AddDenunciaHasTestigo(
					testigo.id_denuncia,
					id_testigo,
					(err) => {
						callback(err);
					}
				);
			} else {
				callback(err);
			}
		}
	);
}

function AddDenunciaHasTestigo(id_denuncia, id_testigo, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO denuncia_has_testigo(id_denuncia,id_testigo) VALUES (?,?)`,
		[id_denuncia, id_testigo],
		(err) => {
			callback(err);
		}
	);
}


/**
 * Obtenemos una testigo mediante un id
 *
 * @param {Number} id_testigo
 * @param {Function} callback
 */
function GetTestigoById(id_testigo, callback) {
	let db = GetDB();
	db.all(
		`SELECT * FROM testigo WHERE id_testigo=?`,
		[id_testigo],
		(err, rows) => {
			callback(err, rows);
		}
	);
}


//A REVISAR
/**
 * Sirve para actualizar datos de victima
 * @param {JSON} testigo
 * @param {*} callback
 */
//SE ACTUALIZARA DESDE USUARIO Y SI ES ASI SOLO SE CAMBIARIA DESCRIPCION???
function UpdateTestigoDB(testigo, callback) {
	let db = GetDB();
	db.run(
		`UPDATE testigo
			SET descripcion = ?, nombre = ?, apellido_paterno = ?, apellido_materno = ?, fecha_nac = ?
			WHERE id_testigo = ?;`,
		[
            testigo.descripcion,
			testigo.nombre,
			testigo.apellido_paterno,
			testigo.apellido_materno,
			testigo.fecha_nac,
			testigo.id_testigo
		],
		(err) => callback(err)
	);
}

module.exports = {
	CreateTestigoDB,
	UpdateTestigoDB,
	AddDenunciaHasTestigo,
	GetTestigoById,
};
