//Realizamos el CRUD de alerta:

const { GetDB } = require("../database");

/**
 * Sirve para registrar el alerta C=create
 * @param {JSON} alerta
 * @param {function} callback
 */
function CreateAlertaDB(alerta, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO alerta(latitud,longitud,id_usuario) VALUES (?,?,?)`,
		[
			alerta.latitud,
			alerta.longitud,
			alerta.id_usuario
		],
        (err) => callback(err));
}

/**
 * Sirve para mostrar la tabla alerta R=read
 * @param {JSON} alerta
 * @param {function} callback
 */
function GetAllAlertaDB(callback){
    let db = GetDB();
    db.all(`SELECT coordenadas, es_verdadera, date_alerta, estado FROM alerta`,/*[//, WHERE id_usuario = ?
        //alerta.id_usuario
    ], */
            (err, rows) => callback(err, rows)
    );
}

/**
 * Sirve para actualizar datos de alerta U=update
 * @param {JSON} alerta
 * @param {*} callback
 */
function UpdateAlertaDB(alerta, callback) {
	let db = GetDB();
	db.run(
		`UPDATE alerta
			SET coordenadas = ?, es_verdadera = ?, date_alerta = ?, estado = ?
			WHERE id_alerta = ?;`,
		[
			alerta.coordenadas,
			alerta.es_verdadera,
			alerta.date_alerta,
			alerta.estado,
			alerta.id_alerta,
		],
		(err) => callback(err)
	);
}

/**
 * Sirve para eliminar una alerta D= delete
 * @param {JSON} alerta
 * @param {*} callback
 */
function DeleteAlertaDB(alerta, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM alerta
			WHERE id_alerta = ?;`,
		[
			alerta.id_alerta,
		],
		(err) => callback(err)
	);
}


module.exports = {
    CreateAlertaDB,
    GetAllAlertaDB,
    UpdateAlertaDB,
    DeleteAlertaDB
};