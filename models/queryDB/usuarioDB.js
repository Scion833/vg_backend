const { GetDB } = require("../database");

/**
 * Sirve para registrar el usuario
 * @param {JSON} usuario
 * @param {function} callback
 */
function CreateUsuarioDB(usuario, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO usuario(nombre, apellido_paterno, apellido_materno, ci, extencion, fecha_nacimiento, correo, password, ciudad, genero, direccion, celular, estado_civil, profesion)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?)`,
		[
			usuario.nombre,
			usuario.apellido_paterno,
			usuario.apellido_materno,
			usuario.ci,
			usuario.extencion,
			usuario.fecha_nacimiento,
			usuario.correo,
			usuario.password,
			usuario.ciudad,
			usuario.genero,
			usuario.direccion,
			usuario.celular,
			usuario.estado_civil,
			usuario.profesion,
		],
		function (err) {
			if (err) {
				callback(err);
				return;
			}
			const id_usuario = this.lastID;
			if ((usuario?.id_identidad ?? 0) !== 0) {
				AddUsuarioHasIdentidad(
					id_usuario,
					usuario.id_identidad,
					(err) => {
						callback(err);
					}
				);
			} else {
				callback(err);
			}
		}
	);
}

function AddUsuarioHasIdentidad(id_usuario, id_identidad, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO usuario_has_identidad(id_usuario,id_identidad) VALUES (?,?)`,
		[id_usuario, id_identidad],
		(err) => {
			callback(err);
		}
	);
}

function GetUsuarioByEmailDB(correo, callback) {
	let db = GetDB();
	db.all(
		`SELECT id_usuario id,'usuario' perfil, password FROM usuario
		WHERE correo=?
		UNION 
		SELECT id_admin id,'administrador' perfil, password FROM administrador
		WHERE correo=?`,
		[correo, correo],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

/**
 * Obtenemos un usuario mediante un id
 *
 * @param {Number} id_usuario
 * @param {Function} callback
 */
function GetUsuarioByIdDB(id_usuario, callback) {
	let db = GetDB();
	db.all(
		`SELECT * FROM usuario WHERE id_usuario=?`,
		[id_usuario],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

/**
 * Obtenemos un usuario mediante un id
 *
 * @param {Number} id_usuario
 * @param {Function} callback
 */
function GetUsuarioByIdForAlertDB(id_usuario, callback) {
	let db = GetDB();
	db.all(
		`SELECT nombre,apellido_paterno,apellido_materno,celular FROM usuario WHERE id_usuario=?`,
		[id_usuario],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

/**
 * Sirve para actualizar datos de usuario
 * @param {JSON} usuario
 * @param {*} callback
 */
function UpdateUsuarioDB(usuario, callback) {
	let db = GetDB();
	db.run(
		`UPDATE usuario
			SET ciudad = ?, direccion = ?, celular = ?, estado_civil = ?, profesion = ?
			WHERE id_usuario = ?;`,
		[
			usuario.ciudad,
			usuario.direccion,
			usuario.celular,
			usuario.estado_civil,
			usuario.profesion,
			usuario.id_usuario,
		],
		(err) => callback(err)
	);
}

/**
 * Sirve para eliminar un usuario D= delete
 * @param {JSON} usuario
 * @param {*} callback
 */
function DeleteUsuarioDB(usuario, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM usuario
			WHERE id_usuario = ?;`,
		[usuario.id_usuario],
		(err) => callback(err)
	);
}

function GetAllUsuariosDB(callback) {
	let db = GetDB();
	db.all(
		`SELECT id_usuario,nombre, apellido_paterno, apellido_materno, genero,ciudad,celular, status FROM usuario`,
		(err, rows) => {
			callback(err, rows);
		}
	);
}

module.exports = {
	CreateUsuarioDB,
	GetUsuarioByEmailDB,
	UpdateUsuarioDB,
	AddUsuarioHasIdentidad,
	GetUsuarioByIdDB,
	DeleteUsuarioDB,
	GetAllUsuariosDB,
	GetUsuarioByIdForAlertDB,
};
