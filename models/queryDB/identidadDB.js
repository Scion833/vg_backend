const { GetDB } = require("../database");

/**
 * Con esta funcion puede añadir un registro en la tabla identidad
 *
 *
 * @param {string} sexo
 * @param {Function} callback
 */
function AddIdentidadDB(sexo, callback) {
    let db = GetDB();
    db.run(`INSERT INTO identidad(sexo) VALUES (?)`, [sexo], (err) => {
        callback(err);
    });
}

/**
 *
 * @param {Function} callback
 */
function GetAllIndentidadDB(callback) {
    let db = GetDB();
    db.all(`SELECT id_identidad,sexo FROM identidad`, (err, rows) =>
        callback(err, rows)
    );
}

module.exports = {
    AddIdentidadDB,
    GetAllIndentidadDB,
};
