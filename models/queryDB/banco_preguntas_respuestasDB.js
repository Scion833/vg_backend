//Realizamos el CRUD de BPR:

const { GetDB } = require("../database");

/**
 * Sirve para registrar el orientate C=create
 * @param {JSON} banco_preguntas_respuestas
 * @param {function} callback
 */
function AddBancoPRDB(banco_preguntas_respuestas, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO banco_preguntas_respuestas(pregunta, respuesta)
    VALUES (?, ?)`,
		[
			banco_preguntas_respuestas.pregunta,
			banco_preguntas_respuestas.respuesta,

		],
        (err) => callback(err));
}

/**
 * Sirve para mostrar la tabla BPR R=read
 * @param {JSON} banco_preguntas_respuestas
 * @param {function} callback
 */
function GetAllBancoPRDB(callback){
    let db = GetDB();
    db.all(`SELECT id_bpr, pregunta, respuesta FROM banco_preguntas_respuestas;`, 
            (err, rows) => callback(err, rows)
    );
}

/**
 * Sirve para actualizar datos de BPR U=update
 * @param {JSON} banco_preguntas_respuestas
 * @param {*} callback
 */
function UpdateBancoPRDB(banco_preguntas_respuestas, callback) {
	let db = GetDB();
	db.run(
		`UPDATE banco_preguntas_respuestas
			SET pregunta = ?, respuesta = ?
			WHERE id_bpr = ?;`,
		[
			banco_preguntas_respuestas.pregunta,
			banco_preguntas_respuestas.respuesta,
			banco_preguntas_respuestas.id_bpr,
		],
		(err) => callback(err)
	);
}

/**
 * Sirve para eliminar un BPR D= delete
 * @param {JSON} banco_preguntas_respuestas
 * @param {*} callback
 */
function DeleteBancoPRDB(banco_preguntas_respuestas, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM banco_preguntas_respuestas
			WHERE id_bpr = ?;`,
		[
			banco_preguntas_respuestas.id_bpr,
		],
		(err) => callback(err)
	);
}


module.exports = {
    AddBancoPRDB,
    GetAllBancoPRDB,
    UpdateBancoPRDB,
    DeleteBancoPRDB,

};