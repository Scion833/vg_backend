const { GetDB } = require("../database");

/**
 * Sirve para registrar el denuncia
 * @param {JSON} agresor
 * @param {function} callback
 */
function AddAgresorDB(agresor, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO agresor (descripcion, nombre, apellido_paterno, apellido_materno)
    VALUES (?, ?, ?, ?)`,
		[
			agresor.descripcion,
			agresor.nombre,
			agresor.apellido_paterno,
			agresor.apellido_materno,
		],
		function (err) {
			if (err) {
				callback(err);
			} else {
				let id_agresor = this.lastID;
				AddDenunciaHasAgresorDB(
					agresor.id_denuncia,
					id_agresor,
					(e) => {
						callback(e);
					}
				);
			}
		}
	);
}

function AddDenunciaHasAgresorDB(id_denuncia, id_agresor, callback) {
	let db = GetDB();
	db.run(
		`INSERT OR IGNORE INTO denuncia_has_agresor(id_denuncia,id_agresor) VALUES (?,?) `,
		[id_denuncia, id_agresor],
		(err) => {
			callback(err);
		}
	);
}

function GetAgresorByIdDenunciaDB(agresor, callback) {
	let db = GetDB();
	db.all(
		`SELECT ROW_NUMBER() OVER (ORDER BY a.id_agresor) AS num,
	a.id_agresor,a.nombre,a.apellido_paterno,a.apellido_materno,a.descripcion FROM agresor a
	INNER JOIN denuncia_has_agresor dha ON dha.id_agresor=a.id_agresor 
	AND dha.id_denuncia=?
	LIMIT ? OFFSET ?`,
		[agresor.id_denuncia, agresor.cantidad, agresor.desde],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetCantidadAgresoByIdDenunciaDB(id_denuncia, callback) {
	let db = GetDB();
	db.all(
		`SELECT count(*) cantidad FROM agresor a
	INNER JOIN denuncia_has_agresor dha ON dha.id_agresor=a.id_agresor 
	AND dha.id_denuncia=?`,
		[id_denuncia],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

module.exports = {
	AddAgresorDB,
	AddDenunciaHasAgresorDB,
	GetAgresorByIdDenunciaDB,
	GetCantidadAgresoByIdDenunciaDB,
};
