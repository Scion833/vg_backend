const { GetDB } = require("../database");


/**
 * Sirve para registrar el administrador
 * @param {JSON} administrador
 * @param {function} callback
 */
function CreateAdministradorDB(administrador, callback) {
    let db = GetDB();
	
    db.run(
        `INSERT INTO administrador(ci_nit, nombre, apellido_paterno, apellido_materno, nro_telefono, correo, password)
	VALUES (?, ?, ?, ?, ?, ?, ?)`,
        [
            administrador.ci_nit,
            administrador.nombre,
            administrador.apellido_paterno,
            administrador.apellido_materno,
            administrador.nro_telefono,
            administrador.correo,
            administrador.password,
        ],
        (err) => {
            callback(err);
        }
    );

}

/**
 * Sirve para actualizar el administrador
 * @param {JSON} administrador
 * @param {function} callback
 */
function UpdateAdministradorDB(administrador, callback) {
    let db = GetDB();
	
    db.run(
        `UPDATE administrador SET ci_nit = ?, nombre = ?, apellido_paterno = ?, apellido_materno = ? , nro_telefono = ? , correo = ?, password = ?
	WHERE id_admin = ?`,
        [
            administrador.ci_nit,
            administrador.nombre,
            administrador.apellido_paterno,
            administrador.apellido_materno,
            administrador.nro_telefono,
            administrador.correo,
            administrador.password,
            administrador.id_admin,
        ],
        (err) => {
            callback(err);
        }
    );
}

function GetAdministradorByEmailDB(correo, callback) {
    let db = GetDB();
    db.all(
        `SELECT * FROM administrador
	WHERE correo=?`,
        [correo],
        (err, rows) => {
            callback(err, rows);
        }
    );
}

function GetAdministradorByIdDB(administrador, callback) {
    let db = GetDB();
    db.all(
        `SELECT adm.id_admin, eve.nombre, eve.fecha_inicio, eve.fecha_final, eve.descripcion, eve.horarios
        FROM administrador adm
        INNER JOIN administrador_has_evento has ON has.id_admin = adm.id_admin
        INNER JOIN evento eve ON eve.id_evento = has.id_evento
        WHERE adm.id_admin = ?
        ORDER BY eve.id_evento DESC`,
        [administrador.id_admin],
        (err, rows) => {
            callback(err, rows);
        }
    );
}

function GetAdministradorAllEventoDB(administrador, callback) {
    let db = GetDB();
    db.all(
        `SELECT eve.nombre, eve.fecha_inicio, eve.fecha_final, eve.descripcion, eve.horarios
        FROM evento eve
        ORDER BY eve.id_evento DESC`,
        (err, rows) => {
            callback(err, rows);
        }
    );
}

module.exports = {
    CreateAdministradorDB,
    GetAdministradorByEmailDB,
    UpdateAdministradorDB,
    GetAdministradorByIdDB,
    GetAdministradorAllEventoDB,
};
