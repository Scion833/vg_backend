//Realizamos el CRUD de orientate:

const { GetDB } = require("../database");

/**
 * Sirve para registrar el orientate C=create
 * @param {JSON} orientate
 * @param {function} callback
 */
function AddOrientateDB(orientate, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO orientate(id_admin, titulo, descripcion, autor, path_multimedia, url_articulo, fecha, estado)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?)`,
		[
			orientate.id_admin,
			orientate.titulo,
			orientate.descripcion,
            orientate.autor,
			orientate.path_multimedia,
			orientate.url_articulo,
			orientate.fecha,
			orientate.estado,

		],
        (err) => callback(err));
}

/**
 * Sirve para mostrar la tabla orientate R=read
 * @param {JSON} orientate
 * @param {function} callback
 */
function GetAllOrientateDB(callback){
    let db = GetDB();
    db.all(`SELECT id_orientate, titulo, descripcion, autor, url_articulo, fecha, estado, id_admin FROM orientate`, 
            (err, rows) => callback(err, rows)
    );
}

/**
 * Sirve para actualizar datos de orientate U=update
 * @param {JSON} orientate
 * @param {*} callback
 */
function UpdateOrientateDB(orientate, callback) {
	let db = GetDB();
	db.run(
		`UPDATE orientate
			SET titulo = ?, descripcion = ?, autor = ?, path_multimedia = ?, url_articulo = ?, fecha = ?, estado = ?
			WHERE id_orientate = ?;`,
		[
			orientate.titulo,
			orientate.descripcion,
			orientate.autor,
			orientate.path_multimedia,
			orientate.url_articulo,
			orientate.fecha,
			orientate.estado,
			orientate.id_orientate,
		],
		(err) => callback(err)
	);
}

/**
 * Sirve para eliminar un orientate D= delete
 * @param {JSON} orientate
 * @param {*} callback
 */
function DeleteOrientateDB(orientate, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM orientate
			WHERE id_orientate = ?;`,
		[
			orientate.id_orientate,
		],
		(err) => callback(err)
	);
}


module.exports = {
    AddOrientateDB,
    GetAllOrientateDB,
    UpdateOrientateDB,
    DeleteOrientateDB,

};