//Realizamos el CRUD de contacto:

const { GetDB } = require("../database");

/**
 * Sirve para registrar el contacto C=create
 * @param {JSON} contacto
 * @param {function} callback
 */
function CreateContactoDB(contacto, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO contacto(nombre, apellido_paterno, apellido_materno, telefono, direccion, parentezco, id_usuario)
    VALUES (?, ?, ?, ?, ?, ?, ?)`,
		[
			contacto.nombre,
			contacto.apellido_paterno,
            contacto.apellido_materno,
			contacto.telefono,
			contacto.direccion,
			contacto.parentezco,
			contacto.id_usuario,
		],
        (err) => callback(err));
}

/**
 * Sirve para mostrar la tabla contacto R=read
 * @param {JSON} contacto
 * @param {function} callback
 */
function GetAllContactoDB(callback){
    let db = GetDB();
    db.all(`SELECT nombre, apellido_paterno, apellido_materno, telefono, direccion FROM contacto`,/*[//, WHERE id_usuario = ?
        //contacto.id_usuario
    ], */
            (err, rows) => callback(err, rows)
    );
}

/**
 * Sirve para mostrar la tabla contacto R=read
 * @param {JSON} contacto
 * @param {function} callback
 */
function GetAllContactoByUserDB(id_usuario, callback){
    let db = GetDB();
    db.all(`SELECT id_contacto, nombre, apellido_paterno, apellido_materno, telefono, direccion, parentezco FROM contacto 
			WHERE id_usuario=?`,
			[id_usuario],
            (err, rows) => callback(err, rows)
    );
}

/**
 * Sirve para actualizar datos de contacto U=update
 * @param {JSON} contacto
 * @param {*} callback
 */
function UpdateContactoDB(contacto, callback) {
	let db = GetDB();
	db.run(
		`UPDATE contacto
			SET nombre = ?, apellido_paterno = ?, apellido_materno = ?, telefono = ?, direccion = ?, parentezco=?
			WHERE id_contacto = ?;`,
		[
			contacto.nombre,
			contacto.apellido_paterno,
			contacto.apellido_materno,
			contacto.telefono,
			contacto.direccion,
			contacto.parentezco,
			contacto.id_contacto,
		],
		(err) => callback(err)
	);
}

/*function AddUsuarioHasContacto(id_usuario, id_contacto, parentezco, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO usuario_has_contacto(id_usuario,id_contacto, parentezco) VALUES (?,?,?)`,
		[id_usuario, id_contacto, parentezco],
		(err) => {
			callback(err);
		}
	);
}*/


/**
 * Sirve para eliminar un Contacto D= delete
 * @param {JSON} Contacto
 * @param {*} callback
 */
function DeleteContactoDB(contacto, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM contacto
			WHERE id_contacto = ?;`,
		[contacto.id_contacto],
		(err) => callback(err)
	);
}


module.exports = {
    CreateContactoDB,
	GetAllContactoDB,
	GetAllContactoByUserDB,
	UpdateContactoDB,
	DeleteContactoDB,
    //AddUsuarioHasContacto
};