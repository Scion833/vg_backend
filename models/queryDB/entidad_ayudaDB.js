//Realizamos el CRUD de entidad_ayuda:

const { GetDB } = require("../database");

/**
 * Sirve para registrar el entidad_ayuda C=create
 * @param {JSON} entidad_ayuda
 * @param {function} callback
 */
function CreateEntidad_AyudaDB(entidad_ayuda, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO entidad_ayuda(id_admin, nombre, telefono, direccion, correo, tipo, latitud, longitud)
    VALUES (?, ?, ?, ?, ?,?,?,?)`,
		[
			entidad_ayuda.id_admin,
			entidad_ayuda.nombre,
			entidad_ayuda.telefono,
			entidad_ayuda.direccion,
			entidad_ayuda.correo,
			entidad_ayuda.tipo,
			entidad_ayuda.latitud,
			entidad_ayuda.longitud,
		],
		(err) => callback(err)
	);
}

function AddDenuncia_Has_EntidadDB(body, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO denuncia_has_entidad(id_denuncia,id_entidad) VALUES (?,?)`,
		[body.id_denuncia, body.id_entidad],
		(err) => callback(err)
	);
}

/**
 * Sirve para mostrar la tabla entidad_ayuda R=read
 * @param {JSON} entidad_ayuda
 * @param {function} callback
 */
function GetAllEntidad_AyudaDB(callback) {
	let db = GetDB();
	db.all(
		`SELECT id_entidad, nombre, telefono, direccion, correo, tipo, latitud, longitud FROM entidad_ayuda`,
		(err, rows) => callback(err, rows)
	);
}

/**
 * Sirve para mostrar la tabla entidad_ayuda con el parametro deseado
 * @param {JSON} entidad_ayuda
 * @param {function} callback
 */
function GetAllEntidad_AyudaByIdAdminDB(entidad_ayuda, callback) {
	let db = GetDB();
	db.all(
		`SELECT id_entidad, nombre, telefono, direccion, correo, tipo FROM entidad_ayuda WHERE id_admin= ?`,
		[entidad_ayuda.id_admin],
		(err, rows) => callback(err, rows)
	);
}

/**
 * Sirve para mostrar la tabla entidad_ayuda con el parametro deseado
 * @param {JSON} entidad_ayuda
 * @param {function} callback
 */
function GetAllEntidad_AyudaByIdDenunciaDB(entidad_ayuda, callback) {
	let db = GetDB();
	db.all(
		`SELECT e.id_entidad, e.nombre, e.telefono, e.direccion, e.correo, e.tipo
	FROM entidad_ayuda e, (select id_entidad
							from denuncia_has_entidad 
							where id_denuncia = ?) tpm
	WHERE e.id_entidad = tpm.id_entidad`,
		[entidad_ayuda.id_denuncia],
		(err, rows) => callback(err, rows)
	);
}

/**
 * Sirve para mostrar la tabla entidad_ayuda con el parametro deseado
 * @param {JSON} entidad_ayuda
 * @param {function} callback
 */
function GetAllEntidad_AyudaNotIdDenunciaDB(entidad_ayuda, callback) {
	let db = GetDB();
	db.all(
		`SELECT e.id_entidad, e.nombre, e.telefono, e.direccion, e.correo, e.tipo
	FROM entidad_ayuda e
	WHERE e.id_entidad NOT IN (select id_entidad
							from denuncia_has_entidad 
							where id_denuncia = ?)`,
		[entidad_ayuda.id_denuncia],
		(err, rows) => callback(err, rows)
	);
}

/**
 * Sirve para actualizar datos de entidad_ayuda U=update
 * @param {JSON} entidad_ayuda
 * @param {*} callback
 */
function UpdateEntidad_AyudaDB(entidad_ayuda, callback) {
	let db = GetDB();
	db.run(
		`UPDATE entidad_ayuda
			SET nombre = ?, telefono = ?, direccion = ?, correo = ?, tipo = ?, latitud = ?, longitud = ?
			WHERE id_entidad = ?;`,
		[
			entidad_ayuda.nombre,
			entidad_ayuda.telefono,
			entidad_ayuda.direccion,
			entidad_ayuda.correo,
			entidad_ayuda.tipo,
			entidad_ayuda.latitud,
			entidad_ayuda.longitud,
			entidad_ayuda.id_entidad,
		],
		(err) => callback(err)
	);
}

/**
 * Sirve para eliminar una entidad_ayuda D= delete
 * @param {JSON} entidad_ayuda
 * @param {*} callback
 */
function DeleteEntidad_AyudaDB(entidad_ayuda, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM entidad_ayuda
			WHERE id_entidad = ?;`,
		[entidad_ayuda.id_entidad],
		function (err) {
			if (err) {
				callback(err);
				return;
			}
			if (entidad_ayuda.id_entidad !== 0) {
				DeleteEntidad_has_DenunciaDB(
					entidad_ayuda.id_entidad,
					(err) => {
						callback(err);
					}
				);
			} else {
				callback(err);
			}
		}
	);
}
function DeleteEntidad_has_DenunciaDB(entidad_ayuda, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM denuncia_has_entidad
			WHERE id_entidad = ?;`,
		[entidad_ayuda.id_entidad],
		(err) => callback(err)
	);
}

module.exports = {
	CreateEntidad_AyudaDB,
	GetAllEntidad_AyudaDB,
	UpdateEntidad_AyudaDB,
	DeleteEntidad_AyudaDB,
	AddDenuncia_Has_EntidadDB,
	GetAllEntidad_AyudaByIdAdminDB,
	GetAllEntidad_AyudaByIdDenunciaDB,
	GetAllEntidad_AyudaNotIdDenunciaDB,
};
