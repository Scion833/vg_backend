const { GetDB } = require("../database");

function AddEventoDB(evento, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO evento (fecha_inicio, nombre, lugar, fecha_final, descripcion, horarios, estado)
        VALUES (?, ?, ?, ?, ?, ?, 1)`,
		[
			evento.fecha_inicio,
			evento.nombre,
			evento.lugar,
			evento.fecha_final,
			evento.descripcion,
			evento.horarios,
		],
		function (err) {
			if (err) {
				callback(err);
				return;
			}

			const id_evento = this.lastID;

			if (evento.id_admin !== 0) {
				AddAdministradorHasEventoDB(
					evento.id_admin,
					id_evento,
					(err) => {
						callback(err);
					}
				);
			} else {
				callback(err);
			}
		}
	);
}

function AddAdministradorHasEventoDB(id_admin, id_evento, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO administrador_has_evento(id_admin, id_evento) VALUES (?,?)`,
		[id_admin, id_evento],
		(err) => {
			callback(err);
		}
	);
}

function GetEventoDB(evento, callback) {
	let db = GetDB();
	db.all(
		`SELECT nombre, fecha_inicio, fecha_final, descripcion, horarios FROM evento WHERE id_evento = ?`,
		[evento.id_evento],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetAllEventoDB(callback) {
	let db = GetDB();
	db.all(
		`SELECT eve.id_evento, eve.lugar, eve.nombre, eve.fecha_inicio, eve.fecha_final, eve.descripcion, eve.horarios,eve.estado
        FROM evento eve
        ORDER BY eve.id_evento DESC`,
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetEventsToAttendDB(id_usuario, callback) {
	let db = GetDB();
	db.all(
		`SELECT DISTINCT eve.id_evento, eve.lugar, eve.nombre, eve.fecha_inicio, 
		eve.fecha_final, eve.descripcion, eve.horarios, uhe.id_usuario
			FROM evento eve
			LEFT JOIN usuario_has_evento uhe ON uhe.id_evento=eve.id_evento
			AND uhe.id_usuario=?
			WHERE eve.estado='1'
			ORDER BY eve.id_evento DESC`,
		[id_usuario],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function AddUsuarioHasEventoDB(id_usuario, id_evento, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO usuario_has_evento(id_usuario,id_evento) VALUES (?,?)`,
		[id_usuario, id_evento],
		(err) => {
			callback(err);
		}
	);
}

function DeleteUsuarioHasEventoDB(id_usuario, id_evento, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM usuario_has_evento
	WHERE id_usuario=? AND id_evento=?`,
		[id_usuario, id_evento],
		(err) => callback(err)
	);
}

function GetUsuarioByEventoDB(id_evento, callback) {
	let db = GetDB();
	db.all(
		`SELECT u.nombre,u.apellido_paterno,u.apellido_materno,u.ci,u.extencion,u.celular FROM usuario u
	INNER JOIN usuario_has_evento uhe ON uhe.id_usuario=u.id_usuario AND uhe.id_evento=?`,
		[id_evento],
		(err, rows) => callback(err, rows)
	);
}

function UpdateEventoStateDB(estado, id_evento, callback) {
	let db = GetDB();
	db.run(
		`UPDATE evento SET estado=?
	WHERE id_evento=?`,
		[estado, id_evento],
		(err) => callback(err)
	);
}

module.exports = {
	AddEventoDB,
	GetEventoDB,
	AddAdministradorHasEventoDB,
	GetAllEventoDB,
	AddUsuarioHasEventoDB,
	GetEventsToAttendDB,
	GetUsuarioByEventoDB,
	DeleteUsuarioHasEventoDB,
	UpdateEventoStateDB,
};
