//Realizamos el CRUD de articulo:

const { GetDB } = require("../database");

/**
 * Sirve para registrar el articulo C=create
 * @param {JSON} articulo
 * @param {function} callback
 */
function CreateArticuloDB(articulo, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO articulo(id_admin, titulo, descripcion, autor, url_imagen, url_articulo, fecha, estado)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?)`,
		[
            articulo.id_admin,
			articulo.titulo,
			articulo.descripcion,
            articulo.autor,
			articulo.url_imagen,
			articulo.url_articulo,
			articulo.fecha,
			1,
		],
        (err) => callback(err));
}

/**
 * Sirve para mostrar la tabla articulo R=read
 * @param {JSON} articulo
 * @param {function} callback
 */
function GetAllArticuloDB(callback){
    let db = GetDB();
    db.all(`SELECT id_articulo, titulo, descripcion, autor, url_imagen, url_articulo, fecha, estado FROM articulo`, 
            (err, rows) => callback(err, rows)
    );
}

/**
 * Sirve para actualizar datos de articulo U=update
 * @param {JSON} articulo
 * @param {*} callback
 */
function UpdateArticuloDB(articulo, callback) {
	let db = GetDB();
	db.run(
		`UPDATE articulo
			SET titulo = ?, descripcion = ?, autor = ?, url_articulo = ?, fecha = ?
			WHERE id_articulo = ?;`,
		[
			articulo.titulo,
			articulo.descripcion,
			articulo.autor,
			articulo.url_articulo,
			articulo.fecha,
			articulo.id_articulo,
		],
		(err) => callback(err)
	);
}

/**
 * Sirve para eliminar un articulo D= delete
 * @param {JSON} articulo
 * @param {*} callback
 */
function DeleteArticuloDB(articulo, callback) {
	let db = GetDB();
	db.run(
		`DELETE FROM articulo
			WHERE id_articulo = ?;`,
		[
			articulo.id_articulo,
		],
		(err) => callback(err)
	);
}


module.exports = {
    CreateArticuloDB,
    GetAllArticuloDB,
    UpdateArticuloDB,
    DeleteArticuloDB,

};