const { GetDB } = require("../database");

/**
 * Sirve para registrar el denuncia
 * @param {JSON} denuncia
 * @param {function} callback
 */
function CreateDenunciaDB(denuncia, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO denuncia(tipo, descripcion, ubicacion, fecha, seguimiento)
    VALUES (?, ?, ?, ?, 1)`,
		[
			denuncia.tipo,
			denuncia.descripcion,
			denuncia.ubicacion,
			denuncia.fecha,
		],
		function (err) {
			if (err) {
				callback(err);
			} else {
				let id_denuncia = this.lastID;
				denuncia.es_testigo = "0";
				AddDenunciaHasUsuarioDB(denuncia, id_denuncia, (e) => {
					callback(e);
				});
			}
		}
	);
}

function AddDenunciaHasUsuarioDB(denuncia, id_denuncia, callback) {
	let db = GetDB();
	db.run(
		`INSERT OR IGNORE INTO denuncia_has_usuario(id_denuncia,id_usuario,es_testigo,anonimo) 
		VALUES (?,?,?,?)`,
		[
			id_denuncia,
			denuncia.id_usuario,
			denuncia.es_testigo,
			denuncia.anonimo,
		],
		(err) => {
			callback(err);
		}
	);
}

function GetDenunciaDB(denuncia, callback) {
	let db = GetDB();
	db.all(
		`SELECT id_denuncia,tipo,descripcion,ubicacion,fecha,seguimiento FROM denuncia
	LIMIT ? OFFSET ?`,
		[denuncia.cantidad, denuncia.desde],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetDenunciaByIdUsuarioDB(denuncia, callback) {
	let db = GetDB();
	db.all(
		`SELECT ROW_NUMBER() OVER (ORDER BY d.id_denuncia) num,d.id_denuncia,d.tipo,d.descripcion,d.ubicacion,d.fecha,
	d.seguimiento,dhu.es_testigo,dhu.anonimo FROM denuncia d
	INNER JOIN denuncia_has_usuario dhu ON dhu.id_denuncia= d.id_denuncia 
	AND dhu.id_usuario=?
	LIMIT ? OFFSET ?`,
		[denuncia.id_usuario, denuncia.cantidad, denuncia.desde],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetCantidadDenunciaByUsuarioDB(id_usuario, callback) {
	let db = GetDB();
	db.all(
		`select count(*) cantidad FROM 
	(SELECT d.id_denuncia,d.tipo,d.descripcion,d.ubicacion,d.fecha,
	d.seguimiento,dhu.es_testigo,dhu.anonimo FROM denuncia d
	INNER JOIN denuncia_has_usuario dhu ON dhu.id_denuncia= d.id_denuncia 
	AND dhu.id_usuario=?)`,
		[id_usuario],
		(err, rows) => {
			callback(err, rows);
		}
	);
}
function UpdateDenunciaDB(denuncia, callback) {
	let db = GetDB();
	db.all(
		`UPDATE denuncia SET tipo = ?, descripcion = ?, ubicacion = ?, fecha = ?, seguimiento = ?
		WHERE id_denuncia = ?`,
		[
			denuncia.tipo,
			denuncia.descripcion,
			denuncia.ubicacion,
			denuncia.fecha,
			denuncia.seguimiento,
			denuncia.id_denuncia,
		],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

module.exports = {
	CreateDenunciaDB,
	GetDenunciaDB,
	AddDenunciaHasUsuarioDB,
	GetDenunciaByIdUsuarioDB,
	GetCantidadDenunciaByUsuarioDB,
	UpdateDenunciaDB,
};

//cambios
