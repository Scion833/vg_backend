const { GetDB } = require("../database");

/**
 * Sirve para registrar victima
 * @param {JSON} victima
 * @param {function} callback
 */
function CreateVictimaDB(victima, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO victima(descripcion, nombre, apellido_paterno, apellido_materno, fecha_nac)
    VALUES (?, ?, ?, ?, ?)`,
		[
			victima.descripcion,
			victima.nombre,
			victima.apellido_paterno,
			victima.apellido_materno,
			victima.fecha_nac,
		],
		function (err) {
			if (err) {
				callback(err);
				return;
			}
			const id_victima = this.lastID;
			if (victima.id_denuncia !== 0) {
				AddDenunciaHasVictima(
					victima.id_denuncia,
					id_victima,
					(err) => {
						callback(err);
					}
				);
			} else {
				callback(err);
			}
		}
	);
}

function AddDenunciaHasVictima(id_denuncia, id_victima, callback) {
	let db = GetDB();
	db.run(
		`INSERT INTO denuncia_has_victima(id_denuncia,id_victima) VALUES (?,?)`,
		[id_denuncia, id_victima],
		(err) => {
			callback(err);
		}
	);
}


/**
 * Obtenemos una victima mediante un id
 *
 * @param {Number} id_victima
 * @param {Function} callback
 */
function GetVictimaById(id_victima, callback) {
	let db = GetDB();
	db.all(
		`SELECT * FROM victima WHERE id_victima=?`,
		[id_victima],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetVictimaByIDdenunciaDB(victima, callback) {
	let db = GetDB();
	db.all(
		`SELECT ROW_NUMBER() OVER (ORDER BY v.id_victima) AS num, v.id_victima,v.descripcion,
				v.nombre,v.apellido_paterno,
				v.apellido_materno,v.fecha_nac
			FROM victima v
			INNER JOIN denuncia_has_victima dhv ON dhv.id_victima=v.id_victima
			AND dhv.id_denuncia=?
			LIMIT ? OFFSET ?`,
		[victima.id_denuncia, victima.cantidad, victima.desde],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

function GetCantidadVictimaByIDdenunciaDB(id_denuncia, callback) {
	let db = GetDB();
	db.all(
		`SELECT count(*) cantidad
	FROM victima v
   INNER JOIN denuncia_has_victima dhv ON dhv.id_victima=v.id_victima
   AND dhv.id_denuncia=?`,
		[id_denuncia],
		(err, rows) => {
			callback(err, rows);
		}
	);
}

//A REVISAR
/**
 * Sirve para actualizar datos de victima
 * @param {JSON} victima
 * @param {*} callback
 */
//SE ACTUALIZARA DESDE USUARIO Y SI ES ASI SOLO SE CAMBIARIA DESCRIPCION???
function UpdateVictimaDB(victima, callback) {
	let db = GetDB();
	db.run(
		`UPDATE victima
			SET descripcion = ?, nombre = ?, apellido_paterno = ?, apellido_materno = ?, fecha_nac = ?
			WHERE id_victima = ?;`,
		[
			victima.descripcion,
			victima.nombre,
			victima.apellido_paterno,
			victima.apellido_materno,
			victima.fecha_nac,
			victima.id_victima
		],
		(err) => callback(err)
	);
}

module.exports = {
	CreateVictimaDB,
	UpdateVictimaDB,
	AddDenunciaHasVictima,
	GetVictimaById,
	GetVictimaByIDdenunciaDB,
	GetCantidadVictimaByIDdenunciaDB,
};
