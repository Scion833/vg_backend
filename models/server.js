const express = require("express");
const cors = require("cors");
const path = require("path");
const fs = require("fs");
const fileUpload = require("express-fileupload");
const { InitDB } = require("./database");
const {
	CreateUsuario,
	LoginUsuario,
	UpdateUsuario,
	GetAllUsuarios,
	GetUsuarioByIDForAlert,
} = require("./api/usuario");
const { validateJWT } = require("./middleware/authentication");
const {
	UpdateAdministrador,
	CreateAdministrador,
	GetAdministradorById,
} = require("./api/administrador");
const { AddIdentidad, GetAllIndentidad } = require("./api/identidad");

const {
	AddDenuncia,
	GetDenuncia,
	UpdateDenuncia,
	GetDenunciaByIdUsuario,
} = require("./api/denuncia");

//Cambios Prueba
const { AddVictima, GetVictimaByIDdenuncia } = require("./api/victima");
const {
	AddPrueba,
	GetPruebaByIdUsuarioAndDenuncia,
	AddDenunciaHasPrueba,
	GetPruebaAgregadaByIdUsuarioAndDenuncia,
	DeleteDenunciaHasPrueba,
} = require("./api/prueba");
const { AddTestigo } = require("./api/testigo");
const {
	CreateEntidad_Ayuda,
	GetAllEntidad_Ayuda,
	UpdateEntidad_Ayuda,
	DeleteEntidad_Ayuda,
	GetAllEntidad_AyudaByIdAdmin,
	AddDenuncia_Has_Entidad,
	GetAllEntidad_AyudaByIdDenuncia,
	GetAllEntidad_AyudaNotIdDenuncia,
} = require("./api/entidad_ayuda");
const { AddAgresor, GetAgresorByIdDenuncia } = require("./api/agresor");

//Articulo
const {
	CreateArticulo,
	GetAllArticulo,
	UpdateArticulo,
} = require("./api/articulo");

//Orientate
const {
	AddOrientate,
	GetAllOrientate,
	UpdateOrientate,
} = require("./api/orientate");

//contacto
const {
	CreateContacto,
	GetAllContacto,
	GetAllContactoByUser,
	UpdateContacto,
	DeleteContacto,	
} = require("./api/contacto");
//alerta
const {
	CreateAlerta,
	GetAllAlerta,
	UpdateAlerta,
	DeleteAlerta,
	EmitAlert,
} = require("./api/alerta");
const {
	AddEvento,
	GetEvento,
	GetAllEvento,
	AddUsuarioHasEvento,
	GetEventsToAttend,
	DeleteUsuarioHasEvento,
	GetUsuarioByEvento,
	HabilitarEvento,
	DeshabilitarEvento,
} = require("./api/evento");

//BAnco Preg-Resp
const {
	AddBancoPR,
	GetAllBancoPR,
	UpdateBancoPR,
	DeleteBancoPR,
} = require("./api/banco_preguntas_respuestas");

class Server {
	constructor(pathPublic, port, dirFiles) {
		this.app = express();
		this.pathPublic = pathPublic;
		this.port = port;
		this.createDir(dirFiles.database);
		InitDB();
		this.createDir(dirFiles.uploads);
		this.createDir(dirFiles.imgMenu);
		this.createDir(dirFiles.pruebas);

		this.middlewares();
		this.routes();
		this.server = require("http").createServer(this.app);
		this.io = require("socket.io")(this.server, {
			cors: {
				origin: "*",
			},
		});
		this.sockets();
	}

	createDir(dir) {
		if (!fs.existsSync(dir)) {
			fs.mkdirSync(dir);
		}
	}

	middlewares() {
		// CORS

		const corsOptions = {
			origin: "*",
			optionsSuccessStatus: 200,
		};

		this.app.use(cors(corsOptions));

		// Directorio Público
		this.app.use(express.static(this.pathPublic));
		this.app.use(fileUpload());

		this.app.use(express.json());
	}

	routes() {
		//usuario
		this.app.post("/api/v1/registrar", CreateUsuario);
		this.app.put("/api/v1/updateUsuario", [UpdateUsuario]);
		this.app.get("/api/v1/getAllUsuarios", [validateJWT, GetAllUsuarios]);
		this.app.post("/api/v1/loginUsuario", [LoginUsuario]);
		this.app.get("/api/v1/getUsuarioByIDForAlert", [
			validateJWT,
			GetUsuarioByIDForAlert,
		]);
		//administrador
		this.app.post("/api/v1/createAdministrador", [CreateAdministrador]);
		this.app.put("/api/v1/updateAdministrador", [UpdateAdministrador]);
		this.app.post("/api/v1/GetAdministradorById", [GetAdministradorById]);
		//identidad
		this.app.post("/api/v1/addIdentidad", [AddIdentidad]);
		this.app.get("/api/v1/getAllIndentidad", [GetAllIndentidad]);

		//AGREGADO VICTIMA PRUEBA Y TESTIGO
		this.app.post("/api/v1/addVictima", [AddVictima]);
		this.app.post("/api/v1/addTestigo", [AddTestigo]);
		this.app.post("/api/v1/addPrueba", [validateJWT, AddPrueba]);
		this.app.post("/api/v1/addDenunciaHasPrueba", [
			validateJWT,
			AddDenunciaHasPrueba,
		]);
		this.app.delete("/api/v1/deleteDenunciaHasPrueba", [
			validateJWT,
			DeleteDenunciaHasPrueba,
		]);
		this.app.post("/api/v1/getVictimaByIDdenuncia", [
			GetVictimaByIDdenuncia,
		]);
		this.app.post("/api/v1/getPruebaByIdUsuarioAndDenuncia", [
			validateJWT,
			GetPruebaByIdUsuarioAndDenuncia,
		]);
		this.app.post("/api/v1/getPruebaAgregadaByIdUsuarioAndDenuncia", [
			validateJWT,
			GetPruebaAgregadaByIdUsuarioAndDenuncia,
		]);
		//denuncia
		this.app.post("/api/v1/addDenuncia", [validateJWT, AddDenuncia]);
		this.app.post("/api/v1/addAgresor", [AddAgresor]);
		this.app.post("/api/v1/getDenuncia", [GetDenuncia]);
		this.app.post("/api/v1/getDenunciaByIdUsuario", [
			validateJWT,
			GetDenunciaByIdUsuario,
		]);

		this.app.post("/api/vi/getAgresorByIdDenuncia", [
			GetAgresorByIdDenuncia,
		]);
		this.app.post("/api/v1/UpdateDenuncia", [UpdateDenuncia]);
		//evento
		this.app.post("/api/v1/AddEvento", [validateJWT, AddEvento]);
		this.app.post("/api/v1/GetEvento", [GetEvento]);
		this.app.get("/api/v1/GetAllEvento", [GetAllEvento]);
		this.app.post("/api/v1/getUsuarioByEvento", [
			validateJWT,
			GetUsuarioByEvento,
		]);
		this.app.get("/api/v1/getEventsToAttend", [
			validateJWT,
			GetEventsToAttend,
		]);
		this.app.post("/api/v1/addUsuarioHasEvento", [
			validateJWT,
			AddUsuarioHasEvento,
		]);
		this.app.delete("/api/v1/deleteUsuarioHasEvento", [
			validateJWT,
			DeleteUsuarioHasEvento,
		]);
		this.app.put("/api/v1/habilitarEvento", [validateJWT, HabilitarEvento]);
		this.app.put("/api/v1/deshabilitarEvento", [
			validateJWT,
			DeshabilitarEvento,
		]);

		//Agregado Articulo y Orientate
		this.app.post("/api/v1/createArticulo", [validateJWT, CreateArticulo]);
		this.app.get("/api/v1/getAllArticulo", [GetAllArticulo]);
		this.app.put("/api/v1/updateArticulo", [validateJWT, UpdateArticulo]);

		//this.app.get("/api/v1/getAllArticulo", [GetAllArticulo]);
		this.app.put("/api/v1/updateArticulo", [UpdateArticulo]);
		this.app.post("/api/v1/addOrientate", [AddOrientate]);
		this.app.get("/api/v1/getAllOrientate", [GetAllOrientate]);
		this.app.put("/api/v1/updateOrientate", [UpdateOrientate]);

		//entidad ayuda
		this.app.post("/api/v1/createEntidad_Ayuda", [
			validateJWT,
			CreateEntidad_Ayuda,
		]);
		this.app.get("/api/v1/getAllEntidad_Ayuda", [GetAllEntidad_Ayuda]);
		this.app.get("/api/v1/getAllEntidad_AyudaByIdAdmin", [
			GetAllEntidad_AyudaByIdAdmin,
		]);
		this.app.get("/api/v1/getAllEntidad_AyudaByIdDenuncia", [
			GetAllEntidad_AyudaByIdDenuncia,
		]);
		this.app.get("/api/v1/getAllEntidad_AyudaNotIdDenuncia", [
			GetAllEntidad_AyudaNotIdDenuncia,
		]);

		this.app.put("/api/v1/updateEntidad_Ayuda", [UpdateEntidad_Ayuda]);
		this.app.delete("/api/v1/deleteEntidad_Ayuda", [DeleteEntidad_Ayuda]);

		this.app.post("/api/v1/addDenuncia_Has_Entidad", [
			AddDenuncia_Has_Entidad,
		]);
		//contacto
		this.app.post("/api/v1/createContacto", [validateJWT, CreateContacto]);
		this.app.get("/api/v1/getAllContacto", [GetAllContacto]);
		this.app.get("/api/v1/getAllContactoByUser", [
			validateJWT,
			GetAllContactoByUser,
		]);
		this.app.put("/api/v1/updateContacto", [
			validateJWT,
			UpdateContacto
		]);
		this.app.delete("/api/v1/deleteContacto",[DeleteContacto]);
		//alerta
		this.app.post("/api/v1/createAlerta", [CreateAlerta]);
		this.app.get("/api/v1/getAllAlerta", [GetAllAlerta]);
		this.app.post("/api/v1/emitAlert", [
			validateJWT,
			(req = express.request, res = express.response) => {
				EmitAlert(req, res, this.io);
			},
		]);

		//Banco Preg-Resp
		this.app.post("/api/v1/addBancoPR", [AddBancoPR]);
		this.app.get("/api/v1/getAllBancoPR", [GetAllBancoPR]);
		this.app.put("/api/v1/updateBancoPR", [UpdateBancoPR]);
		this.app.delete("/api/v1/deleteBancoPR", [DeleteBancoPR]);

		//////ip
		this.app.get("/getIp", (req, res) => {
			res.json({
				ip: GetIP(),
			});
		});
		//this.app.post('/api/addItem', UploadFile);

		//#####################################################
		//#####################################################
		this.app.get("/uploads/pruebas", [
			validateJWT,
			(req, res) => {
				let usuario = req?.data?.usuario;
				let p = path.join(
					__dirname,
					"../uploads",
					"pruebas",
					usuario.id + "",
					req.query?.nfile
				);
				if (fs.existsSync(p)) {
					res.sendFile(p);
				} else {
					res.sendStatus(404);
				}
			},
		]);
		this.app.get("/uploads/articulos", (req, res) => {
			let p = path.join(
				__dirname,
				"../uploads",
				"articulos",
				req.query?.nameImage
			);
			if (fs.existsSync(p)) {
				res.sendFile(p);
			} else {
				res.sendStatus(404);
			}
		});
		this.app.get("/uploads/imgMenu", (req, res) => {
			let p = path.join(
				__dirname,
				"../uploads",
				"imgMenu",
				req.query.img
			);
			if (fs.existsSync(p)) {
				res.sendFile(p);
			} else {
				res.sendStatus(404);
			}
		});
		this.app.get("*", (req, res) => {
			res.sendFile(this.pathPublic.concat("/index.html"));
		});
	}

	sockets() {
		this.io.on("connection", (socket) => {
			//console.log("cliente conectado", socket.id);

			socket.on("disconnect", () => {
				//console.log("cliente desconectado");
			});
		});
	}

	listen() {
		this.server.listen(this.port, () => {
			console.log("run in port ", this.port);
		});
	}
}

module.exports = Server;
