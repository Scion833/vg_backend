const { request, response } = require("express");
const {
    CreateTestigoDB,
} = require("../queryDB/testigoDB");

function AddTestigo(req = request, res = response) {
    let body = req.body;
     CreateTestigoDB(body, (err) => {
        if (err) {
            console.log(err);
            res.status(406).json({
                mensaje: "Error al guardar en la tabla Testigo",
            });
        } else {
            res.status(200).json({ mensaje: "Se guardo correctamente" });
        }
    }); 
}

module.exports = {
    AddTestigo,
};
