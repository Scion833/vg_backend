const { response, request } = require("express");

const {
	AddOrientateDB,
    GetAllOrientateDB,
    UpdateOrientateDB,
    DeleteOrientateDB,
} = require("../queryDB/orientateDB");

function AddOrientate(req = request, res = response) {
	let body = req.body;
	AddOrientateDB(body, (err) => {
        if (err) {
            console.log(err);
            res.status(406).json({
                mensaje: "Error al guardar en la tabla orientate",
            });
        } else {
            res.status(200).json({ mensaje: "Se guardo correctamente" });
        }
    });
}

function GetAllOrientate(req = request, res = response) {
    GetAllOrientateDB((err, rows) => {
        if (err) {
            res.status(409).json({
                mensaje: "Error al obtener datos de orientate",
            });
        } else {
            res.status(200).json({ data: rows });
        }
    });
}

function UpdateOrientate(req = request, res = response) {
	let body = req.body;
	UpdateOrientateDB(body, (err) => {
		if (err) {
			res.status(409).json({
				mensaje:
					"Hubo un error al realizar cambios en la base de datos",
                    err
			});
		} else {
			res.status(200).json({ mensaje: "datos actualizados!!!!!!!!" });
		}
	});
}

function DeleteOrientate(req = request, res = response){
    let body = req.body;
    DeleteOrientateDB(body, (err) =>{
            if (err) {
                res.status(409).json({
                    mensaje:
                        "Hubo un error al realizar cambios en la base de datos",
                });
            } else {
                res.status(200).json({ mensaje: "datos actualizados, Orientate Elminado!" });
            }
    });
}

module.exports = {
    AddOrientate,
    GetAllOrientate,
    UpdateOrientate,
    DeleteOrientate,

}