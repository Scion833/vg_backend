const { request, response } = require("express");
const {
	AddAgresorDB,
	GetAgresorByIdDenunciaDB,
	GetCantidadAgresoByIdDenunciaDB,
} = require("../queryDB/agresorDB");

function AddAgresor(req = request, res = response) {
	let body = req.body;
	AddAgresorDB(body, (err) => {
		if (err) {
			console.log(err);
			res.status(406).json({
				mensaje: "Error al guardar en la tabla agresor",
			});
		} else {
			res.status(200).json({ mensaje: "Se guardo correctamente" });
		}
	});
}

function GetAgresorByIdDenuncia(req = request, res = response) {
	let body = req.body;
	GetAgresorByIdDenunciaDB(body, (err, rows) => {
		if (err) {
			console.log(err);
			res.status(406).json({
				mensaje: "Hubo un error al obtener datos de los agresores",
				err,
			});
		} else {
			GetCantidadAgresoByIdDenunciaDB(body.id_denuncia, (e, r) => {
				if (e) {
					console.log(e);
					res.status(406).json({
						mensaje:
							"Hubo un error al obtener datos de los agresores",
						err: e,
					});
				} else {
					res.status(200).json({
						mensaje:
							"Datos de los agresores de la denuncia obtenidos correctamente",
						cantidad: r[0].cantidad,
						data: rows,
					});
				}
			});
		}
	});
}

module.exports = {
	AddAgresor,
	GetAgresorByIdDenuncia,
};
