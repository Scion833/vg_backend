const { response, request } = require("express");

const {
	AddBancoPRDB,
    GetAllBancoPRDB,
    UpdateBancoPRDB,
    DeleteBancoPRDB,
} = require("../queryDB/banco_preguntas_respuestasDB");

function AddBancoPR(req = request, res = response) {
	let body = req.body;
	AddBancoPRDB(body, (err) => {
        if (err) {
            console.log(err);
            res.status(406).json({
                mensaje: "Error al guardar en la tabla Preg-Preg",
            });
        } else {
            res.status(200).json({ mensaje: "Se guardo correctamente!!!" });
        }
    });
}

function GetAllBancoPR(req = request, res = response) {
	GetAllBancoPRDB((err, rows) => {
		if (err) {
			res.status(409).json({
				mensaje: "Error al obtener datos de Banco PR",
			});
		} else {
			res.status(200).json({ data: rows });
		}
	});
}

function UpdateBancoPR(req = request, res = response) {
	let body = req.body;
	UpdateBancoPRDB(body, (err) => {
		if (err) {
			res.status(409).json({
				mensaje:
					"Hubo un error al realizar cambios en la base de datos",
                    err
			});
		} else {
			res.status(200).json({ mensaje: "datos actualizados!!!!!!!!" });
		}
	});
}

function DeleteBancoPR(req = request, res = response){
    let body = req.body;
    DeleteBancoPRDB(body, (err) =>{
            if (err) {
                res.status(409).json({
                    mensaje:
                        "Hubo un error al realizar cambios en la base de datos",
                });
            } else {
                res.status(200).json({ mensaje: "datos actualizados, Preg-Resp Elminado!" });
            }
    });
}

module.exports = {
    AddBancoPR,
    GetAllBancoPR,
    UpdateBancoPR,
    DeleteBancoPR,

}