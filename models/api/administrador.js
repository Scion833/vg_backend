const { response, request } = require("express");
//se creo automaticamente el create y el get
const {
	CreateAdministradorDB,
	UpdateAdministradorDB,
	GetAdministradorByEmailDB,
	GetAdministradorByIdDB,
} = require("../queryDB/administradorDB");
const bcrypt = require("bcrypt");
const { generateJWT } = require("../tools/tools");

function CreateAdministrador(req = request, res = response) {
	let body = req.body;

	let pwd = body.password;
	bcrypt
		.hash(pwd, 10)
		.then((hash) => {
			body.password = hash;
			CreateAdministradorDB(body, (err) => {
				if (err) {
					res.status(409).json({
						err,
						mensaje: "Error al registrar en la base de datos",
					});
				} else {
					res.status(201).json({
						mensaje: "Se registro correctamente",
					});
				}
			});
		})
		.catch((err) => {
			console.log(err);
			res.status(409).json({ mensaje: "Error al encriptar" });
		});
}

function LoginAdministrador(req = request, res = response) {
	let body = req.body;
	let correo = body.correo;
	let password = body.password;

	GetAdministradorByEmailDB(correo, (err, rows) => {
		if (err) {
			res.status(409).json({ mensaje: "Error al obtener los datos" });
		} else {
			if (rows.length > 0) {
				bcrypt.compare(password, rows[0].password).then((esIgual) => {
					if (esIgual) {
						generateJWT({
							perfil: "administrador",
							idUsuario: rows[0].id_admin,
						})
							.then((token) => {
								res.status(200).json({ "x-token": token });
							})
							.catch((err) => {
								console.log(err);
								res.status(409).json({
									mensaje: "Error al generar token",
								});
							});
					} else {
						res.status(401).json({ mensaje: "Error al loguear" });
					}
				});
			} else {
				res.status(404).json({ mensaje: "Admin no encontrado" });
			}
		}
	});
}

function UpdateAdministrador(req = request, res = response) {
	let body = req.body;
	let pwd = body.password;
	bcrypt
		.hash(pwd, 10)
		.then((hash) => {
			body.password = hash;

			UpdateAdministradorDB(body, (err) => {
				if (err) {
					res.status(400).json({ message: err.message });
				} else {
					res.status(200).json({
						mensaje: "Se actualizo correctamente",
					});
				}
			});
		})
		.catch((err) => {
			res.status(409).json({ mensaje: "Error al encriptar" });
		});
}

function GetAdministradorById(req = request, res = request) {
    let body = req.body;
    GetAdministradorByIdDB(body, (err, rows) => {
        if (err) {
            console.log(err);
            res.status(409).json({
                mensaje: "Error al obtener evento(s)",
                err,
            });
        } else {
            res.status(200).json({
				mensaje: "Obtenido",
				data: rows,
			});
        }
    });
}

function GetAdministradorById(req = request, res = request) {
    let body = req.body;
    GetAdministradorByIdDB(body, (err, rows) => {
        if (err) {
            console.log(err);
            res.status(409).json({
                mensaje: "Error al obtener evento(s)",
                err,
            });
        } else {
            res.status(200).json({
				mensaje: "Obtenido",
				data: rows,
			});
        }
    });
}

module.exports = {
	CreateAdministrador,
	LoginAdministrador,
	UpdateAdministrador,
	GetAdministradorById,
};

//HOLAAAAAAA
