const { request, response } = require("express");
const {
	CreateDenunciaDB,
	GetDenunciaDB,
	GetDenunciaByIdUsuarioDB,
	GetCantidadDenunciaByUsuarioDB,
} = require("../queryDB/denunciaDB");

function AddDenuncia(req = request, res = response) {
	let body = req.body;
	let usuario = req.data?.usuario;
	body.id_usuario = usuario.id;
	CreateDenunciaDB(body, (err) => {
		if (err) {
			console.log(err);
			res.status(406).json({
				mensaje: "Error al guardar en la tabla Denuncia",
			});
		} else {
			res.status(200).json({ mensaje: "Se guardo correctamente" });
		}
	});
}

function GetDenuncia(req = request, res = response) {
	let body = req.body;
	GetDenunciaDB(body, (err, rows) => {
		if (err) {
			res.status(409).json({
				mensaje: "Error al obtener denuncias",
				err,
			});
		} else {
			res.status(200).json({
				mensaje: "Obtenido",
				data: rows,
			});
		}
	});
}

function GetDenunciaByIdUsuario(req = request, res = response) {
	let body = req.body;
	let usuario = req.data?.usuario;
	if (usuario) {
		body.id_usuario = usuario.id;
		GetDenunciaByIdUsuarioDB(body, (err, rows) => {
			if (err) {
				res.status(409).json({
					mensaje: "Error al obtener las denuncias",
					err,
				});
			} else {
				GetCantidadDenunciaByUsuarioDB(usuario.id, (e, r) => {
					if (e) {
						res.status(409).json({
							mensaje: "Error al obtener las denuncias",
							e,
						});
					} else {
						res.status(200).json({
							mensaje: "obtenido",
							cantidad: r[0].cantidad,
							data: rows,
						});
					}
				});
			}
		});
	} else {
		res.status(412).json({
			msg: "No hizo la peticion como usuario",
		});
	}
}

function UpdateDenuncia(req = request, res = response) {
	let body = req.body;
	UpdateDenunciaDB(body, (err, rows) => {
		if (err) {
			res.status(409).json({
				mensaje: "Error al modificar el registro.",
				err,
			});
		} else {
			res.status(200).json({
				mensaje: "Registro modificado.",
				data: rows,
			});
		}
	});
}

module.exports = {
	AddDenuncia,
	GetDenuncia,
	UpdateDenuncia,
	GetDenunciaByIdUsuario,
};
