const { response, request } = require("express");

const{  CreateContactoDB,
        GetAllContactoDB,
	    GetAllContactoByUserDB,
	    UpdateContactoDB,
        DeleteContactoDB,
}=require("../queryDB/contactoDB");

function CreateContacto(req = request, res = response) {
    let body = req.body;
    let usuario = req.data?.usuario;
    body.id_usuario=usuario.id;
    
    CreateContactoDB(body, (err) => {
        if (err) {
            console.log(err);
            res.status(406).json({
                mensaje: "Error al guardar el contacto",
            });
        } else {
            res.status(200).json({ mensaje: "Se guardo Contacto correctamente" });
        }
    });
}

function GetAllContacto(req = request, res = response) {
    let body = req.body;
    GetAllContactoDB((err, rows) => {
        if (err) {
            res.status(409).json({
                mensaje: "Error al obtener datos de contactos",
            });
        } else {
            res.status(200).json({ data: rows });
        }
    });
}

function GetAllContactoByUser(req = request, res = response) {
    let body = req.body;
    let usuario = req.data?.usuario;
    if(usuario)
    {
        body.id_usuario=usuario.id;
        GetAllContactoByUserDB(body.id_usuario, (err, rows) => {
            if (err) {
                res.status(409).json({
                    mensaje: "Error al obtener datos del contacto",
                });
            } else {
                res.status(200).json({ data: rows });
            }
        });
    }else{
        res.status(401).json({
            mensaje:"usuario no valido",
        });
    }
}

function UpdateContacto(req = request, res = response) {
	let body = req.body;
    let usuario = req.data?.usuario;
    if(usuario){
        body.id_usuario=usuario.id;
        UpdateContactoDB(body, (err) => {
            if (err) {
                res.status(409).json({
                    mensaje:
                        "Hubo un error al realizar cambios en la base de datos",
                });
            } else {
                res.status(200).json({ mensaje: "contacto actualizado!!!!!!!!" });
            }
        });
    }else{
        res.status(401).json({
            mensaje:"usuario no valido",
        });
    }
	
}

function DeleteContacto(req = request, res=response){
    let body=req.body;
        DeleteContactoDB(body, (err) =>{
            if (err) {
                res.status(409).json({
                    mensaje:
                        "Hubo un error al eliminar el contacto",
                });
            } else {
                res.status(200).json({ mensaje: "contacto eliminado!!!" });
            }
        });
}


module.exports = {
    CreateContacto,
    GetAllContacto,
    GetAllContactoByUser,
    UpdateContacto,
    DeleteContacto,
};