const { response, request } = require("express");

const {
	CreateAlertaDB,
	GetAllAlertaDB,
	UpdateAlertaDB,
	DeleteAlertaDB,
} = require("../queryDB/alertaDB");

function CreateAlerta(req = request, res = response) {
	let body = req.body;
	CreateAlertaDB(body, (err) => {
		if (err) {
			console.log(err);
			res.status(406).json({
				mensaje: "Error al guardar en la tabla alerta",
			});
		} else {
			res.status(200).json({ mensaje: "Se guardo Alerta correctamente" });
		}
	});
}

function GetAllAlerta(req = request, res = response) {
	GetAllEntidad_AyudaDB((err, rows) => {
		if (err) {
			res.status(409).json({
				mensaje: "Error al obtener datos de Alerta",
			});
		} else {
			res.status(200).json({ data: rows });
		}
	});
}

function UpdateAlerta(req = request, res = response) {
	let body = req.body;
	UpdateAlertaDB(body, (err) => {
		if (err) {
			res.status(409).json({
				mensaje:
					"Hubo un error al realizar cambios en la base de datos",
			});
		} else {
			res.status(200).json({
				mensaje: "datos Alerta actualizados!!!!!!!!",
			});
		}
	});
}

function DeleteAlerta(req = request, res = response) {
	let body = req.body;
	DeleteAlertaDB(body, (err) => {
		if (err) {
			res.status(409).json({
				mensaje:
					"Hubo un error al realizar cambios en la base de datos (Eliminar alerta)",
			});
		} else {
			res.status(200).json({
				mensaje: "datos actualizados, Alerta Elminada!",
			});
		}
	});
}

function EmitAlert(req = request, res = response, io) {
	let body = req.body;
	let usuario = req.data?.usuario;
	if (usuario) {
		body.id_usuario = usuario.id;
		console.log(body.latitud, body.longitud, body.mensaje);
		CreateAlertaDB(body, (err) => {
			if (err) {
				res.status(409).json({
					mensaje:
						"Error al intentar crear la alerta, aun asi se envio la alerta a todos los administradores conectados",
				});
			} else {
				res.status(200).json({
					mensaje:
						"Se guardo la alerta y se envio a los administradores conectados",
				});
			}
		});
		io.emit("emitAlert", {
			latitud: body.latitud,
			longitud: body.longitud,
			mensaje: body.mensaje,
			id_usuario: body.id_usuario,
		});
		/* res.status(200).json({
			mensaje: "Se envio correctamente",
		}); */
	} else {
		res.status(401).json({
			mensaje: "solo los usuarios registrados pueden hacer una alerta",
		});
	}
}

module.exports = {
	CreateAlerta,
	GetAllAlerta,
	UpdateAlerta,
	DeleteAlerta,
	EmitAlert,
};
