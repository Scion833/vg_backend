const { request, response } = require("express");
const fs = require("fs");
const path = require("path");
const {
	CreatePruebaDB,
	GetPruebaByIdUsuarioAndDenunciaDB,
	AddDenunciaHasPruebaDB,
	GetPruebaAgregadaByIdUsuarioAndDenunciaDB,
	DeleteDenunciaHasPruebaDB,
	GetCantidadPruebasDB,
	GetCantidadPruebasAgregadasDB,
} = require("../queryDB/pruebaDB");

const path_pruebas = "/uploads/pruebas";

function AddPrueba(req = request, res = response) {
	let body = req.body;
	let usuario = req?.data?.usuario;

	let archivo = req.files?.archivo;
	if (usuario) {
		let dirPruebas = path.join(
			__dirname,
			"../../",
			path_pruebas,
			usuario.id + ""
		);
		createDir(dirPruebas);
		let url_file = path.join(dirPruebas, archivo.name);
		archivo.mv(url_file, (err) => {
			if (err) {
				return res.status(409).json({
					msg: "erro al almacenar el archivo: " + archivo.name,
					err,
				});
			} else {
				body.url_file = archivo.name;
				body.id_usuario = usuario.id;
				CreatePruebaDB(body, (err) => {
					if (err) {
						console.log(err);
						res.status(406).json({
							mensaje: "Error al guardar en la tabla Prueba",
						});
					} else {
						res.status(200).json({
							mensaje: "Se guardo correctamente",
						});
					}
				});
			}
		});
	} else {
		res.send(409).json({ msg: "Error al obtener datos del cliente" });
	}
}

function GetPruebaByIdUsuarioAndDenuncia(req = request, res = response) {
	let body = req.body;
	let usuario = req?.data?.usuario;
	if (usuario) {
		body.id_usuario = usuario.id;
		GetPruebaByIdUsuarioAndDenunciaDB(body, (err, rows) => {
			if (err) {
				res.status(409).json({
					msg: "Error al obtener los datos",
					err,
				});
			} else {
				GetCantidadPruebasDB(
					body.id_denuncia,
					body.id_usuario,
					(e, r) => {
						if (e) {
							res.status(409).json({
								msg: "Error al obtener los datos pruebas",
								e,
							});
						} else {
							res.status(200).json({
								msg: "Se obtuvieron los datos",
								cantidad: r[0].cantidad,
								data: rows,
							});
						}
					}
				);
			}
		});
	} else {
		res.status(412).json({
			msg: "Pre condicion fallida, su sesion no cumple con los parametros que se necesitan",
		});
	}
}

function GetPruebaAgregadaByIdUsuarioAndDenuncia(
	req = request,
	res = response
) {
	let body = req.body;
	let usuario = req?.data?.usuario;
	if (usuario) {
		body.id_usuario = usuario.id;
		GetPruebaAgregadaByIdUsuarioAndDenunciaDB(body, (err, rows) => {
			if (err) {
				res.status(409).json({
					msg: "Error al obtener los datos",
					err,
				});
			} else {
				GetCantidadPruebasAgregadasDB(body.id_denuncia,body.id_usuario,(e,r)=>{
					if(e){
						res.status(409).json({
							msg: "Error al obtener los datos cantidad",
							err,
						});
					}else{
						res.status(200).json({
							msg: "Se obtuvieron los datos",
							cantidad:r[0].cantidad,
							data: rows,
						});

					}
				})
			}
		});
	} else {
		res.status(412).json({
			msg: "Pre condicion fallida, su sesion no cumple con los parametros que se necesitan",
		});
	}
}

function AddDenunciaHasPrueba(req = request, res = response) {
	let body = req.body;
	AddDenunciaHasPruebaDB(body.id_denuncia, body.id_prueba, (err) => {
		if (err) {
			res.status(409).json({
				msg: "Error al vincular la prueba",
				err,
			});
		} else {
			res.status(200).json({
				msg: "Se Vinculo correctamente",
			});
		}
	});
}

function createDir(dir) {
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}
}

function DeleteDenunciaHasPrueba(req = request, res = response) {
	let body = req.body;
	DeleteDenunciaHasPruebaDB(body.id_denuncia, body.id_prueba, (err) => {
		if (err) {
			res.status(409).json({
				msg: "Error al Eliminar datos",
				err,
			});
		} else {
			res.status(200).json({
				msg: "Se elimino el vinculo correctamente ",
			});
		}
	});
}

module.exports = {
	AddPrueba,
	GetPruebaByIdUsuarioAndDenuncia,
	AddDenunciaHasPrueba,
	GetPruebaAgregadaByIdUsuarioAndDenuncia,
	DeleteDenunciaHasPrueba,
};
