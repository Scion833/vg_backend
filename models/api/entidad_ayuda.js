const { response, request } = require("express");

const {
	CreateEntidad_AyudaDB,
    GetAllEntidad_AyudaDB,
    UpdateEntidad_AyudaDB,
    DeleteEntidad_AyudaDB,
    AddDenuncia_Has_EntidadDB,
    GetAllEntidad_AyudaByIdAdminDB,
    GetAllEntidad_AyudaByIdDenunciaDB,
    GetAllEntidad_AyudaNotIdDenunciaDB,
} = require("../queryDB/entidad_ayudaDB");

function CreateEntidad_Ayuda(req = request, res = response) {
	let body = req.body;
    let administrador = req.data?.administrador;
    body.id_admin=administrador.id;

	CreateEntidad_AyudaDB(body, (err) => {
        if (err) {
            console.log(err);
            res.status(406).json({
                mensaje: "Error al guardar en la tabla entidad_ayuda",
            });
        } else {
            res.status(200).json({ mensaje: "Se guardo correctamente" });
        }
    });
}

function GetAllEntidad_Ayuda(req = request, res = response) {
    let body = req.body;
    GetAllEntidad_AyudaDB((err, rows) => {
        if (err) {
            res.status(409).json({
                mensaje: "Error al obtener datos de entidad_ayuda",
            });
        } else {
            res.status(200).json({ data: rows });
        }
    });
}

function GetAllEntidad_AyudaByIdAdmin(req = request, res = response) {
    let body = req.body;
    GetAllEntidad_AyudaByIdAdminDB(body, (err, rows) => {
        if (err) {
            res.status(409).json({
                mensaje: "Error al obtener datos de entidad_ayuda",
            });
        } else {
            res.status(200).json({ data: rows });
        }
    });
}

function GetAllEntidad_AyudaNotIdDenuncia(req = request, res = response) {
    let body = req.body;
    GetAllEntidad_AyudaNotIdDenunciaDB(body, (err, rows) => {
        if (err) {
            res.status(409).json({
                mensaje: "Error al obtener datos de entidad_ayuda",
            });
        } else {
            res.status(200).json({ data: rows });
        }
    });
}

function GetAllEntidad_AyudaByIdDenuncia(req = request, res = response) {
    let body = req.body;
    GetAllEntidad_AyudaByIdDenunciaDB(body, (err, rows) => {
        if (err) {
            res.status(409).json({
                mensaje: "Error al obtener datos de entidad_ayuda de la denuncia",
            });
        } else {
            res.status(200).json({ data: rows });
        }
    });
}

function UpdateEntidad_Ayuda(req = request, res = response) {
	let body = req.body;
	UpdateEntidad_AyudaDB(body, (err) => {
		if (err) {
			res.status(409).json({
				mensaje:
					"Hubo un error al realizar cambios en la base de datos",
			});
		} else {
			res.status(200).json({ mensaje: "datos actualizados!!!!!!!!" });
		}
	});
}

function DeleteEntidad_Ayuda(req = request, res = response){
    let body = req.body;
    DeleteEntidad_AyudaDB(body, (err) =>{
            if (err) {
                res.status(409).json({
                    mensaje:
                        "Hubo un error al realizar cambios en la base de datos",
                });
            } else {
                res.status(200).json({ mensaje: "datos actualizados, Entidad_Ayuda Elminada!" });
            }
    });
}

function AddDenuncia_Has_Entidad(req = request, res = response){
    let body = req.body;
    AddDenuncia_Has_EntidadDB(body, (err) =>{
            if (err) {
                res.status(409).json({
                    mensaje:
                        "Hubo un error al añadir la Entidad Ayuda con la Denuncia",
                });
            } else {
                res.status(200).json({ mensaje: "datos actualizados, Entidad_Ayuda con su Denuncia!" });
            }
    });

}

module.exports = {
    CreateEntidad_Ayuda,
    GetAllEntidad_Ayuda,
    UpdateEntidad_Ayuda,
    DeleteEntidad_Ayuda,
    AddDenuncia_Has_Entidad,
    GetAllEntidad_AyudaByIdAdmin,
    GetAllEntidad_AyudaByIdDenuncia,
    GetAllEntidad_AyudaNotIdDenuncia

}


//holaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa