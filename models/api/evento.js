const { request, response } = require("express");
const {
	AddEventoDB,
	GetEventoDB,
	GetAllEventoDB,
	AddUsuarioHasEventoDB,
	GetEventsToAttendDB,
	DeleteUsuarioHasEventoDB,
	GetUsuarioByEventoDB,
	UpdateEventoStateDB,
} = require("../queryDB/eventoDB");

function AddEvento(req = request, res = response) {
	let body = req.body;
	let administrador = req.data?.administrador;
	body.id_admin = administrador.id;

	AddEventoDB(body, (err) => {
		if (err) {
			console.log(err);
			res.status(406).json({
				mensaje: "Error al guardar en la tabla evento",
			});
		} else {
			res.status(200).json({ mensaje: "Se guardo correctamente" });
		}
	});
}

function GetEvento(req = request, res = response) {
	let body = req.body;
	GetEventoDB(body, (err, rows) => {
		if (err) {
			console.log(err);
			res.status(409).json({
				mensaje: "Error al obtener el evento",
				err,
			});
		} else {
			res.status(200).json({
				mensaje: "Obtenido",
				data: rows,
			});
		}
	});
}

function GetAllEvento(req = request, res = response) {
	GetAllEventoDB((err, rows) => {
		if (err) {
			console.log(err);
			res.status(409).json({
				mensaje: "Error al obtener el evento",
				err,
			});
		} else {
			res.status(200).json({
				mensaje: "Obtenido",
				data: rows,
			});
		}
	});
}

function GetEventsToAttend(req = request, res = response) {
	let usuario = req.data?.usuario;
	if (usuario) {
		GetEventsToAttendDB(usuario.id, (err, rows) => {
			if (err) {
				res.status(409).json({
					msg: "Error Al obtener los datos",
					err,
				});
			} else {
				res.status(200).json({
					msg: "Se obtuvieron los datos correctamente",
					data: rows,
				});
			}
		});
	} else {
		res.status(412).json({
			msg: "No hizo la peticion como usuario",
		});
	}
}

function AddUsuarioHasEvento(req = request, res = response) {
	let body = req.body;
	let usuario = req.data?.usuario;
	if (usuario) {
		AddUsuarioHasEventoDB(usuario.id, body.id_evento, (err) => {
			if (err) {
				res.status(409).json({ msg: "Error al Agregar el dato", err });
			} else {
				res.status(200).json({
					msg: "Se Agrego Correctamente",
				});
			}
		});
	} else {
		res.status(412).json({
			msg: "No hizo la peticion como usuario",
		});
	}
}

function DeleteUsuarioHasEvento(req = request, res = response) {
	let body = req.body;
	let usuario = req.data?.usuario;
	if (usuario) {
		DeleteUsuarioHasEventoDB(usuario.id, body.id_evento, (err) => {
			if (err) {
				res.status(409).json({ msg: "Error al Eliminar", err });
			} else {
				res.status(200).json({
					msg: "Se elimino Correctamente",
				});
			}
		});
	} else {
		res.status(412).json({
			msg: "No hizo la peticion como usuario",
		});
	}
}

function GetUsuarioByEvento(req = request, res = response) {
	let body = req.body;
	let administrador = req.data?.administrador;
	if (administrador) {
		GetUsuarioByEventoDB(body.id_evento, (err, rows) => {
			if (err) {
				res.status(409).json({
					msg: "Error al obtener los datos",
					err,
				});
			} else {
				res.status(200).json({
					msg: "Se obtuvieron los datos correctamente",
					data: rows,
				});
			}
		});
	} else {
		res.status(412).json({
			msg: "No hizo la peticion como administrador",
		});
	}
}

function UpdateEventoState(estado, req = request, res = response) {
	let body = req.body;
	let administrador = req.data?.administrador;
	if (administrador) {
		UpdateEventoStateDB(estado, body.id_evento, (err) => {
			if (err) {
				res.status(409).json({
					msg: "Error al actualizar evento",
					err,
				});
			} else {
				res.status(200).json({
					msg: "Se actualizo correctamente",
				});
			}
		});
	} else {
		res.status(412).json({
			msg: "No hizo la peticion como administrador",
		});
	}
}

function HabilitarEvento(req = request, res = response) {
	UpdateEventoState("1", req, res);
}

function DeshabilitarEvento(req = request, res = response) {
	UpdateEventoState("0", req, res);
}

module.exports = {
	AddEvento,
	GetEvento,
	GetAllEvento,
	GetEventsToAttend,
	AddUsuarioHasEvento,
	GetUsuarioByEvento,
	DeleteUsuarioHasEvento,
	HabilitarEvento,
	DeshabilitarEvento,
};
