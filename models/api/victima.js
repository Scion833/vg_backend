const { request, response } = require("express");
const {
	CreateVictimaDB,
	GetVictimaByIDdenunciaDB,
	GetCantidadVictimaByIDdenunciaDB,
} = require("../queryDB/victimaDB");

function AddVictima(req = request, res = response) {
	let body = req.body;
	CreateVictimaDB(body, (err) => {
		if (err) {
			console.log(err);
			res.status(406).json({
				mensaje: "Error al guardar en la tabla Victima",
			});
		} else {
			res.status(200).json({ mensaje: "Se guardo correctamente" });
		}
	});
}

function GetVictimaByIDdenuncia(req = request, res = response) {
	let body = req.body;
	GetVictimaByIDdenunciaDB(body, (err, rows) => {
		if (err) {
			res.status(409).json({
				mensaje: "Error al obtener datos",
				err,
			});
		} else {
			GetCantidadVictimaByIDdenunciaDB(body.id_denuncia, (e, r) => {
				if (e) {
					res.status(409).json({
						mensaje: "Error al obtener datos",
						err: e,
					});
				} else {
					res.status(200).json({
						cantidad: r[0]?.cantidad,
						mensaje: "Se obtuvieron los datos correctamente",
						data: rows,
					});
				}
			});
		}
	});
}

module.exports = {
	AddVictima,
	GetVictimaByIDdenuncia,
};
