const { response, request } = require("express");

const {
	registerDB,
	GetUsuarioByEmailDB,
	CreateUsuarioDB,
	UpdateUsuarioDB,
	DeleteOrientateDB,
	GetAllUsuariosDB,
	GetUsuarioByIdForAlertDB,
} = require("../queryDB/usuarioDB");
const bcrypt = require("bcrypt");
const { generateJWT } = require("../tools/tools");

function CreateUsuario(req = request, res = response) {
	let body = req.body;

	let pwd = body.password;
	bcrypt
		.hash(pwd, 10)
		.then((hash) => {
			body.password = hash;
			CreateUsuarioDB(body, (err) => {
				if (err) {
					res.status(409).json({
						mensaje: "Error al registrar en la base de datos",
					});
				} else {
					res.status(200).json({
						mensaje: "Se registro correctamente",
					});
				}
			});
		})
		.catch((err) => {
			console.log(err);
			res.status(409).json({ mensaje: "Error al encriptar" });
		});
}

function LoginUsuario(req = request, res = response) {
	let body = req.body;
	let correo = body.correo;
	let password = body.password;

	GetUsuarioByEmailDB(correo, async (err, rows) => {
		if (err) {
			res.status(409).json({
				mensaje: "Error al obtener los datos",
				err,
			});
		} else {
			if (rows.length > 0) {
				let perfiles = await generatePerfiles(password, rows);
				if (perfiles.length > 0) {
					generateJWT({
						perfiles: perfiles,
					})
						.then((token) => {
							res.status(200).json({ "x-token": token });
						})
						.catch((err) => {
							res.status(409).json({
								mensaje: "Error al generar token",
							});
						});
				} else {
					res.status(409).json({
						mensaje: "Error en las credenciales",
					});
				}
			} else {
				res.status(404).json({ mensaje: "Usuario no encontrado" });
			}
		}
	});
}

const generatePerfiles = async (myPassword = "", objects = []) => {
	let perfiles = [];
	for (let i = 0; i < objects.length; i++) {
		await bcrypt
			.compare(myPassword, objects[i].password)
			.then((esIgual) => {
				if (esIgual) {
					perfiles.push({
						id: objects[i].id,
						perfil: objects[i].perfil,
					});
				}
			})
			.catch((err) => {
				console.log(err);
			});
	}
	return perfiles;
};

function UpdateUsuario(req = request, res = response) {
	let body = req.body;
	UpdateUsuarioDB(body, (err) => {
		if (err) {
			res.status(409).json({
				mensaje:
					"Hubo un error al realizar cambios en la base de datos",
			});
		} else {
			res.status(200).json({ mensaje: "datos actualizados!!!!!!!!" });
		}
	});
}

function DeleteUsuario(req = request, res = response) {
	let body = req.body;
	DeleteUsuarioDB(body, (err) => {
		if (err) {
			res.status(409).json({
				mensaje:
					"Hubo un error al realizar cambios en la base de datos",
			});
		} else {
			res.status(200).json({
				mensaje: "datos actualizados, Dato Usuario Elminado!",
			});
		}
	});
}

function GetAllUsuarios(req = request, res = response) {
	let administrador = req.data?.administrador;
	if (administrador) {
		GetAllUsuariosDB((err, rows) => {
			if (err) {
				res.status(409).json({
					mensaje: "Error al obtener los datos de la base de datos",
				});
			} else {
				res.status(200).json({
					mensaje: "Datos obtenidos correctamente",
					data: rows,
				});
			}
		});
	} else {
		res.status(401).json({
			mensaje: "Solo los administradores pueden obtener estos datos",
		});
	}
}

function GetUsuarioByIDForAlert(req = request, res = response) {
	let usuario = req.data?.usuario;
	if (usuario) {
		GetUsuarioByIdForAlertDB(usuario.id, (err, rows) => {
			if (err) {
				res.status(409).json({
					mensaje: "Error al obtener el usuario de la base de datos",
				});
			} else {
				if (rows.length > 0) {
					res.status(200).json({
						mensaje: "usuario encontrado",
						data: rows[0],
					});
				} else {
					res.status(404).json({
						mensaje: "usuario no encontrado",
					});
				}
			}
		});
	} else {
		res.status(401).json({
			mensaje: "Necesita ser un usuario para obtener sus datos",
		});
	}
}

//ejemplo de comparar contraseña con hash
/*bcrypt.compare(pwd,"$2b$10$aw5pvowvgax6mPc0FwQmM.6mfX3qGbIGUnHl8uYxuHrsgXW6VIkFK").then(esIgual=>{
	console.log(esIgual)
	if(esIgual){
		console.log('Es valido')
	}else{
		console.log('no es valido')
	}
}).catch(err=>{
	console.log(err)
})*/

module.exports = {
	CreateUsuario,
	LoginUsuario,
	UpdateUsuario,
	DeleteUsuario,
	GetAllUsuarios,
	GetUsuarioByIDForAlert,
};
