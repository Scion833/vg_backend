const { request, response } = require("express");
const {
    AddIdentidadDB,
    GetAllIndentidadDB,
} = require("../queryDB/identidadDB");

function AddIdentidad(req = request, res = response) {
    let body = req.body;
    AddIdentidadDB(body.sexo, (err) => {
        if (err) {
            console.log(err);
            res.status(406).json({
                mensaje: "Error al guardar en la tabla identidad",
            });
        } else {
            res.status(200).json({ mensaje: "Se guardo correctamente" });
        }
    });
}

function GetAllIndentidad(req = request, res = response) {
    GetAllIndentidadDB((err, rows) => {
        if (err) {
            res.status(409).json({
                mensaje: "Error al obtener datos de identidad",
            });
        } else {
            res.status(200).json({ data: rows });
        }
    });
}

module.exports = {
    AddIdentidad,
    GetAllIndentidad,
};
