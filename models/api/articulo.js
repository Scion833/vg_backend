const { response, request } = require("express");
const path = require("path");
const fs = require("fs");

const {
	CreateArticuloDB,
	GetAllArticuloDB,
	UpdateArticuloDB,
	DeleteArticuloDB,
} = require("../queryDB/articuloDB");

function CreateArticulo(req = request, res = response) {
	let body = req.body;
	let administrador = req.data?.administrador;
	let archivo = req.files?.archivo;
	if (archivo) {
		let dirImage = path.join(__dirname, "../../", "uploads", "articulos");
		createDir(dirImage);
		let path_imagen = path.join(dirImage, archivo.name);
		archivo.mv(path_imagen, (err) => {
			if (err) {
				res.status(409).json({
					mensaje: "Error al guardar archivo",
					err,
				});
			} else {
				if (administrador?.id) {
					body.id_admin = administrador.id;
					body.url_imagen = archivo.name;
					CreateArticuloDB(body, (err) => {
						if (err) {
							res.status(406).json({
								mensaje:
									"Error al guardar en la tabla articulo",
								err,
							});
						} else {
							res.status(200).json({
								mensaje: "Se guardo correctamente",
							});
						}
					});
				} else {
					res.status(401).json({
						mensaje:
							"Error al guardar en la tabla articulo, por falta de credenciales",
					});
				}
			}
		});
	} else {
		res.status(412).json({
			mensaje: "Tiene que agregar una imagen",
		});
	}
}

function createDir(dir) {
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}
}

function GetAllArticulo(req = request, res = response) {
	GetAllArticuloDB((err, rows) => {
		if (err) {
			res.status(409).json({
				mensaje: "Error al obtener datos de articulo",
			});
		} else {
			res.status(200).json({ data: rows });
		}
	});
}

function UpdateArticulo(req = request, res = response) {
	let body = req.body;
	let administrador = req.data?.administrador;
	if (administrador) {
		UpdateArticuloDB(body, (err) => {
			if (err) {
				res.status(409).json({
					mensaje:
						"Hubo un error al realizar cambios en la base de datos",
				});
			} else {
				res.status(200).json({ mensaje: "datos actualizados!!!!!!!!" });
			}
		});
	} else {
		res.status(401).json({
			mensaje: "Usuario no autorizado",
		});
	}
}

function DeleteArticulo(req = request, res = response) {
	let body = req.body;
	DeleteArticuloDB(body, (err) => {
		if (err) {
			res.status(409).json({
				mensaje:
					"Hubo un error al realizar cambios en la base de datos",
			});
		} else {
			res.status(200).json({
				mensaje: "datos actualizados, Articulo Elminado!",
			});
		}
	});
}

module.exports = {
	CreateArticulo,
	GetAllArticulo,
	UpdateArticulo,
	DeleteArticulo,
};
