require("dotenv").config();
const path = require("path");
const port = process.env.PORT??80;
let serverStarted = false;

const dirPublic = path.join(__dirname, "public");
const dirFiles = {
	uploads: path.join(__dirname, "uploads"),
	imgMenu: path.join(__dirname, "uploads/imgMenu"),
	pruebas: path.join(__dirname, "uploads/pruebas"),
	database: path.join(__dirname, "database"),
};

const Server = require(path.join(__dirname, "models/server"));

if (!serverStarted) {
	const server = new Server(dirPublic, port, dirFiles);
	server.listen();
	serverStarted = true;
}
