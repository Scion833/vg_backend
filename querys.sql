CREATE TABLE IF NOT EXISTS ADMINISTRADOR (
	id_admin INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	nombre TEXT,
	apellido_paterno TEXT,
	apellido_materno TEXT,
	correo TEXT,
	password TEXT
);
INSERT INTO ADMINISTRADOR(
		nombre,
		apellido_paterno,
		apellido_materno,
		correo
	)
VALUES (
		'DAVID',
		'ROSALES',
		'MANCHEGO',
		'drosales@gmail.com'
	);
INSERT INTO ADMINISTRADOR(
		nombre,
		apellido_paterno,
		apellido_materno,
		correo
	)
VALUES ('RONALD', 'ROMAN', 'PAREDES', 'rroman@gmail.com');
INSERT INTO ADMINISTRADOR(
		nombre,
		apellido_paterno,
		apellido_materno,
		correo
	)
VALUES ('JULIA', 'PINTO', 'CACERES', 'jpinto@gmail.com');
CREATE TABLE IF NOT EXISTS AGRESOR (
	id_agresor INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	descripcion TEXT,
	nombre TEXT,
	apellido_paterno TEXT,
	apellido_materno TEXT
);
INSERT INTO AGRESOR (
		descripcion,
		nombre,
		apellido_paterno,
		apellido_materno
	)
VALUES (
		'Hombre, 30 años aprox, vestimenta ropa casual',
		'Carlos',
		'Torrez',
		'Flores'
	);
INSERT INTO AGRESOR (
		descripcion,
		nombre,
		apellido_paterno,
		apellido_materno
	)
VALUES (
		'Mujer, 25 años aprox, vestimenta ropa formal',
		'Claudia',
		'Copa',
		'Ramos'
	);
INSERT INTO AGRESOR (
		descripcion,
		nombre,
		apellido_paterno,
		apellido_materno
	)
VALUES (
		'Hombre, 20 años aprox, vestimenta ropa estudiante',
		'NN',
		'NN',
		'NN'
	);
CREATE TABLE IF NOT EXISTS USUARIO (
	id_usuario INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	nombre TEXT,
	apellido_paterno TEXT,
	apellido_materno TEXT,
	ci INTEGER,
	extension TEXT,
	fechanac TEXT,
	correo TEXT,
	password TEXT,
	ciudad TEXT,
	genero TEXT,
	direccion TEXT,
	celular INTEGER,
	estado_civil TEXT,
	profesion TEXT
);
INSERT INTO USUARIO (
		nombre,
		apellido_paterno,
		apellido_materno,
		ci,
		extension,
		fechanac,
		correo,
		password,
		ciudad,
		genero,
		direccion,
		celular,
		estado_civil,
		profesion
	)
VALUES (
		'Juan',
		'García',
		'López',
		'1234567',
		'123',
		'1990-05-15',
		'juan.garcia@example.com',
		'pass123',
		'Ciudad de México',
		'Masculino',
		'Calle 123, Colonia Centro',
		'555-123-4567',
		'Soltero',
		'Ingeniero'
	),
	(
		'María',
		'Rodríguez',
		'Pérez',
		'9876543',
		'456',
		'1988-09-22',
		'maria.rodriguez@example.com',
		'securepass',
		'Guadalajara',
		'Femenino',
		'Calle 456, Colonia Norte',
		'555-987-6543',
		'Soltero',
		'Abogado'
	),
	(
		'Alejandro',
		'Martínez',
		'Gómez',
		'5432198',
		'789',
		'1995-02-10',
		'alejandro.martinez@example.com',
		'ale123',
		'Monterrey',
		'Masculino',
		'Avenida 789, Colonia Sur',
		'555-789-0123',
		'Casado',
		'Médico'
	);
CREATE TABLE IF NOT EXISTS ALERTA (
	id_alerta INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	coordenadas TEXT,
	es_verdadera TEXT,
	date_alerta TEXT,
	estado TEXT,
	id_usuario INTEGER,
	FOREIGN KEY (id_usuario) REFERENCES USUARIO (id_usuario)
);
INSERT INTO alerta (
		coordenadas,
		es_verdadera,
		date_alerta,
		estado,
		id_usuario
	)
VALUES (
		'12.345,67.890',
		'true',
		'2023-08-26 10:15:00',
		'pendiente',
		1
	);
INSERT INTO alerta (
		coordenadas,
		es_verdadera,
		date_alerta,
		estado,
		id_usuario
	)
VALUES (
		'23.456,78.901',
		'false',
		'2023-08-26 11:30:00',
		'resuelta',
		2
	);
INSERT INTO alerta (
		coordenadas,
		es_verdadera,
		date_alerta,
		estado,
		id_usuario
	)
VALUES (
		'34.567,89.012',
		'true',
		'2023-08-26 12:45:00',
		'pendiente',
		3
	);
CREATE TABLE IF NOT EXISTS CONTACTO (
	id_contacto INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	nombre TEXT,
	telefono INTEGER,
	direccion TEXT,
	parentezco TEXT
);
INSERT INTO CONTACTO (nombre, telefono, direccion, parentezco)
VALUES (
		'Ana García',
		5551234567,
		'Calle 123, Colonia Centro',
		'Amigo'
	);
INSERT INTO CONTACTO (nombre, telefono, direccion, parentezco)
VALUES (
		'Carlos Rodríguez',
		5559876543,
		'Avenida 456, Colonia Norte',
		'Familiar'
	);
INSERT INTO CONTACTO (nombre, telefono, direccion, parentezco)
VALUES (
		'Luisa Martínez',
		5555555555,
		'Calle 789, Colonia Sur',
		'Colega'
	);
CREATE TABLE IF NOT EXISTS EVENTO (
	id_eventos INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	fecha_inicio TEXT,
	nombre TEXT,
	fecha_final TEXT,
	descripcion TEXT,
	horarios TEXT
);
INSERT INTO evento (
		fecha_inicio,
		nombre,
		fecha_final,
		descripcion,
		horarios
	)
VALUES (
		'2023-09-01',
		'Conferencia de Tecnología',
		'2023-09-03',
		'Conferencia anual sobre las últimas tendencias tecnológicas.',
		'9:00 AM - 5:00 PM'
	);
INSERT INTO evento (
		fecha_inicio,
		nombre,
		fecha_final,
		descripcion,
		horarios
	)
VALUES (
		'2023-10-15',
		'Feria de Arte Local',
		'2023-10-17',
		'Exhibición de arte local en diversas formas y estilos.',
		'10:00 AM - 7:00 PM'
	);
INSERT INTO evento (
		fecha_inicio,
		nombre,
		fecha_final,
		descripcion,
		horarios
	)
VALUES (
		'2023-11-20',
		'Concierto de Rock en Vivo',
		'2023-11-20',
		'Concierto de bandas locales de rock en vivo.',
		'8:00 PM - 12:00 AM'
	);
CREATE TABLE IF NOT EXISTS IDENTIDAD (
	id_identidad INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	sexo TEXT
);
INSERT INTO identidad (sexo)
VALUES ('Masculino');
INSERT INTO identidad (sexo)
VALUES ('Femenino');
INSERT INTO identidad (sexo)
VALUES ('No binario');
CREATE TABLE IF NOT EXISTS ITINERARIO (
	id_itinerario INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	nombre TEXT,
	fecha_inicio TEXT,
	fecha_final TEXT,
	hora_inicio TEXT,
	hora_final TEXT,
	descripcion TEXT,
	id_eventos INTEGER,
	FOREIGN KEY (id_eventos) REFERENCES EVENTO (id_eventos)
);
INSERT INTO itinerario (
		nombre,
		fecha_inicio,
		fecha_final,
		hora_inicio,
		hora_final,
		descripcion,
		id_eventos
	)
VALUES (
		'Itinerario Día 1',
		'2023-09-01',
		'2023-09-01',
		'09:00 AM',
		'05:00 PM',
		'Día de conferencias sobre tecnología.',
		1
	);
INSERT INTO itinerario (
		nombre,
		fecha_inicio,
		fecha_final,
		hora_inicio,
		hora_final,
		descripcion,
		id_eventos
	)
VALUES (
		'Itinerario Día 2',
		'2023-09-02',
		'2023-09-02',
		'10:00 AM',
		'06:00 PM',
		'Más conferencias y talleres interactivos.',
		1
	);
INSERT INTO itinerario (
		nombre,
		fecha_inicio,
		fecha_final,
		hora_inicio,
		hora_final,
		descripcion,
		id_eventos
	)
VALUES (
		'Itinerario Día 1',
		'2023-10-15',
		'2023-10-15',
		'11:00 AM',
		'08:00 PM',
		'Exploración de las exposiciones de arte local.',
		2
	);
CREATE TABLE IF NOT EXISTS ORIENTATE (
	id_orientate INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	titulo TEXT,
	descripcion TEXT,
	autor TEXT,
	path_multimedia TEXT,
	url_articulo TEXT,
	fecha TEXT,
	estado TEXT,
	id_admin INTEGER,
	FOREIGN KEY (id_admin) REFERENCES ADMINISTRADOR (id_admin)
);
INSERT INTO orientate (
		titulo,
		descripcion,
		autor,
		path_multimedia,
		url_articulo,
		fecha,
		estado,
		id_admin
	)
VALUES (
		'Consejos para el éxito',
		'Descubre consejos prácticos para alcanzar tus metas personales y profesionales.',
		'Laura Martínez',
		'/path/archivo1.mp4',
		'https://www.example.com/articulo1',
		'2023-08-27',
		'publicado',
		1
	);
INSERT INTO orientate (
		titulo,
		descripcion,
		autor,
		path_multimedia,
		url_articulo,
		fecha,
		estado,
		id_admin
	)
VALUES (
		'Aprende a manejar el estrés',
		'Conoce técnicas efectivas para manejar el estrés en tu vida diaria.',
		'Carlos Rodríguez',
		'/path/archivo2.mp4',
		'https://www.example.com/articulo2',
		'2023-08-28',
		'publicado',
		2
	);
INSERT INTO orientate (
		titulo,
		descripcion,
		autor,
		path_multimedia,
		url_articulo,
		fecha,
		estado,
		id_admin
	)
VALUES (
		'Construye tu marca personal',
		'Aprende a destacar y crear una marca personal sólida en el mundo profesional.',
		'Ana García',
		'/path/archivo3.mp4',
		'https://www.example.com/articulo3',
		'2023-08-29',
		'pendiente',
		3
	);
CREATE TABLE IF NOT EXISTS PRUEBA (
	id_prueba INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	url_file TEXT,
	descripcion TEXT
);
INSERT INTO prueba (url_file, descripcion)
VALUES (
		'https://www.example.com/archivo1.pdf',
		'Prueba sobre el tema de matemáticas avanzadas.'
	);
INSERT INTO prueba (url_file, descripcion)
VALUES (
		'https://www.example.com/archivo2.pdf',
		'Prueba práctica de programación en Python.'
	);
INSERT INTO prueba (url_file, descripcion)
VALUES (
		'https://www.example.com/archivo3.pdf',
		'Examen sobre literatura contemporánea.'
	);
CREATE TABLE IF NOT EXISTS ROLES_ADMINISTRADOR (
	id_roles_administrador INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	rol TEXT
);
INSERT INTO ROLES_ADMINISTRADOR (rol)
VALUES ('ADM_BD');
INSERT INTO ROLES_ADMINISTRADOR (rol)
VALUES ('ADM_EVENTO');
CREATE TABLE IF NOT EXISTS ROLES_USUARIO (
	id_roles_usuario INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	rol TEXT
);
INSERT INTO ROLES_USUARIO (rol)
VALUES ('USR_TESTIGO');
INSERT INTO ROLES_USUARIO (rol)
VALUES ('USR_EVENTOS');
INSERT INTO ROLES_USUARIO (rol)
VALUES ('USR_VICTIMA');
CREATE TABLE ARTICULOS (
	id_articulos INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	titulo TEXT,
	descripcion TEXT,
	autor TEXT,
	url_imagen TEXT,
	url_articulo TEXT,
	fecha TEXT,
	estado INTEGER,
	id_admin INTEGER,
	FOREIGN KEY (id_admin) REFERENCES ADMINISTRADOR (id_admin)
);
INSERT INTO ARTICULOS (
		titulo,
		descripcion,
		autor,
		url_imagen,
		url_articulo,
		fecha,
		estado,
		id_admin
	)
VALUES (
		'Violencia en las RRSS',
		'Incremento de la violencia en plataformas de interaccion social como bullying',
		'Miguel Ortiz',
		'https://instagram.com/usuario/imagen456.png',
		'https://www.bbc.com/news/world-us-canada-12345678',
		'2023-09-25',
		1,
		2
	);
INSERT INTO ARTICULOS (
		titulo,
		descripcion,
		autor,
		url_imagen,
		url_articulo,
		fecha,
		estado,
		id_admin
	)
VALUES (
		'Causas de el aumento de la violencia',
		'Que origina la violenia',
		'Yohoni Cuenca',
		'https://instagram.com/usuario/imagen456.png',
		'https://www.bbc.com/news/world-us-canada-12345678',
		'2019-01-15',
		0,
		2
	);
INSERT INTO ARTICULOS (
		titulo,
		descripcion,
		autor,
		url_imagen,
		url_articulo,
		fecha,
		estado,
		id_admin
	)
VALUES (
		'Terapias de Desestres',
		'Formas de combatir el estres',
		'Yohoni Cuenca',
		'https://twitter.com/usuario/imagen456.png',
		'https://www.cnn.com/news/world-us-canada-12345678',
		'2015-09-25',
		0,
		1
	);
CREATE TABLE IF NOT EXISTS BANCOPREGUNTASRESPUESTAS (
	id_bpr INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	pregunta TEXT,
	respuesta TEXT
);
INSERT INTO BANCOPREGUNTASRESPUESTAS (pregunta, respuesta)
VALUES (
		'¿Como identificar señales de una relacion toxica?',
		'Falta de respeto y abuso verbal: Insultos, humillaciones, menosprecios y lenguaje ofensivo son señales de una relación tóxica.'
	);
INSERT INTO BANCOPREGUNTASRESPUESTAS (pregunta, respuesta)
VALUES (
		'¿Donde acudir en caso de presenciar violencia?',
		'Instituciones Educativas: Universidades y colegios a menudo ofrecen servicios de asesor.'
	);
INSERT INTO BANCOPREGUNTASRESPUESTAS (pregunta, respuesta)
VALUES (
		'¿Que tipos de violencia existen?',
		'Violencia Física, Violencia Verbal,Violencia Psicológica o Emocional, Violencia Sexual, Violencia Económica, Violencia Digital o Cibernética, Violencia Doméstica, Violencia de Género, Violencia Social o Estructural, Violencia Escolar o Bullying.'
	);
CREATE TABLE DENUNCIA (
	id_denuncia INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	tipo TEXT,
	descripcion TEXT,
	ubicacion TEXT,
	fecha TEXT,
	anonimo TEXT,
	testigo TEXT,
	seguimiento TEXT
);
INSERT INTO DENUNCIA (
		tipo,
		descripcion,
		ubicacion,
		fecha,
		anonimo,
		testigo,
		seguimiento
	)
VALUES (
		'Violencia Laboral',
		'Acoso laboral por parte de un administrativo',
		'-16.502256692833615',
		'2023-05-24',
		'1',
		'1',
		'0'
	);
INSERT INTO DENUNCIA (
		tipo,
		descripcion,
		ubicacion,
		fecha,
		anonimo,
		testigo,
		seguimiento
	)
VALUES (
		'Violencia Familiar',
		'Violencia de pareja',
		'23.502256692833615',
		'2023-05-24',
		'0',
		'0',
		'0'
	);
INSERT INTO DENUNCIA (
		tipo,
		descripcion,
		ubicacion,
		fecha,
		anonimo,
		testigo,
		seguimiento
	)
VALUES (
		'Violencia familiar',
		'Violencia de pareja',
		'23.502256692833615',
		'2023-07-01',
		'1',
		'0',
		'1'
	);
CREATE TABLE VICTIMA (
	id_victima INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	descripcion TEXT,
	nombre TEXT,
	apellido_paterno TEXT,
	apellido_materno TEXT,
	fechanac TEXT
);
INSERT INTO VICTIMA (
		descripcion,
		nombre,
		apellido_paterno,
		apellido_materno,
		fechanac
	)
VALUES (
		'Agresion Fisica',
		'Fernando',
		'Flores',
		'Quispe',
		'1993-05-24'
	);
INSERT INTO VICTIMA (
		descripcion,
		nombre,
		apellido_paterno,
		apellido_materno,
		fechanac
	)
VALUES (
		'Agresion Fisica',
		'Maria',
		'Quispe',
		'Quispe',
		'2005-01-06'
	);
INSERT INTO VICTIMA (
		descripcion,
		nombre,
		apellido_paterno,
		apellido_materno,
		fechanac
	)
VALUES (
		'Bullying',
		'Fernando',
		'Flores',
		'Quispe',
		'1993-05-24'
	);
CREATE TABLE IF NOT EXISTS ENTIDAD_AYUDA (
	id_entidad INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	nombre TEXT,
	telefono INTEGER,
	direccion TEXT,
	correo TEXT,
	coordenadas TEXT,
	id_admin INTEGER,
	FOREIGN KEY (id_admin) REFERENCES ADMINISTRADOR(id_admin)
);
) -- ADMINISTRADOR(id_admin, nombre, apellido_paterno, apellido_materno, correo)
-- AGRESOR (id_agresor, descripcion, nombre, apellido_paterno, apellido_materno)
-- ALERTA (id_alerta, id_usuario, coordenadas, es_verdadera, date_alerta, estado,id_usuario)
-- ARTICULOS  (id_articulos , id_admin, titulo, descripcion, autor, url_imagen, url_articulo, fecha, estado) 
-- BANCOPREGUNTASRESPUESTAS (id_bpr,pregunta, respuesta)
-- CONTACTO(id_contacto, nombre, telefono, direccion, parentezco)
-- DENUNCIA (id_denuncia, tipo, descripcion,  ubicacion, fecha, anonimo, testigo, seguimiento )
-- EVENTO (id_eventos, fecha_inicio, nombre, fecha_final, descripcion, horarios)
-- IDENTIDAD (id_identidad, sexo) 
-- ITINERARIO (id_itinerario, id_evento, nombre, fecha_inicio, fecha final, hora_inicio, hora_final, descripcion)
-- ORIENTATE (id_orientate, id_admin,  titulo, descripcion, autor, path_multimedia, url_articulo, fecha, estado)
-- PRUEBA (id_prueba, url_file, descripcion)
-- ROLES_ADMINISTRADOR (id_roles_administrador, rol)
-- ROLES_USUARIO(id_roles_usuario, rol)
-- USUARIO (Id_usuario, nombre, apellido_paterno, apellido_materno, ci, extension, fechanac, correo, password, ciudad,genero,direccion,celular,estado_civil,profesion)
-- VICTIMA (id_victima , descripcion, nombre, apellido_paterno, apellido_materno, fecha_nac) 
--

--	ALEX
--USUARIO_HAS_CONTACTO (id_usuario, id_contacto)
CREATE TABLE IF NOT EXISTS USUARIO_HAS_CONTACTO (
	id_usuario INTEGER,
	id_contacto INTEGER,
	FOREIGN KEY (id_usuario) REFERENCES USUARIO (id_usuario),
	FOREIGN KEY (id_contacto) REFERENCES CONTACTO (id_contacto)
);
-- ADMINISTRADOR_EVENTOTOS(id_admin, id_even) 
CREATE TABLE IF NOT EXISTS USUARIO_HAS_EVENTO (
	id_usuario INTEGER,
	id_evento INTEGER,
	FOREIGN KEY (id_usuario) REFERENCES USUARIO (id_usuario),
	FOREIGN KEY (id_evento) REFERENCES EVENTO (id_evento)
);
CREATE TABLE IF NOT EXISTS ADMINISTRADOR_HAS_EVENTOS (
	id_admin INTEGER,
	id_evento INTEGER,
	FOREIGN KEY (id_admin) REFERENCES ADMINISTRADOR (id_admin),
	FOREIGN KEY (id_evento) REFERENCES EVENTO (id_evento)
);
-- MARCELO
CREATE TABLE IF NOT EXISTS ROLES_HAS_ADMINISTRADOR (
	id_roles_administrador INTEGER,
	id_admin INTEGER,
	FOREIGN KEY(id_roles_administrador) REFERENCES ROLES_ADMINISTRADOR(id_roles_administrador),
	FOREIGN KEY(id_admin) REFERENCES ADMINISTRADOR(id_admin)
);
INSERT INTO ROLES_HAS_ADMINISTRADOR (id_roles_administrador, id_admin ) VALUES(1,1),(2,1),(2,2),(1,3),(2,3);


-- USUARIO_HAS_IDENTIDAD (id_usuario, id_identidad)
CREATE TABLE IF NOT EXISTS USUARIO_HAS_IDENTIDAD (
	id_usuario INTEGER,
	id_identidad INTEGER,
	FOREIGN KEY(id_usuario) REFERENCES USUARIO(id_usuario),
	FOREIGN KEY(id_identidad) REFERENCES IDENTIDAD(id_identidad)
);
--insertamos datos:
INSERT INTO USUARIO_HAS_IDENTIDAD(id_usuario, id_identidad) VALUES(1,3),(2,2),(3,1); 

-- DENUNCIA_HAS_PRUEBA (id_denuncia, id_prueba)
CREATE TABLE IF NOT EXISTS DENUNCIA_HAS_PRUEBA (
	id_denuncia INTEGER,
	id_prueba INTEGER,
	FOREIGN KEY(id_denuncia) REFERENCES DENUNCIA(id_denuncia),
	FOREIGN KEY(id_prueba) REFERENCES PRUEBA(id_prueba)
);
--insertamos pruebas:
INSERT INTO DENUNCIA_HAS_PRUEBA (id_denuncia, id_prueba ) VALUES (1,1), (2,2), (3,2), (3,3);

---- DENUNCIA_HAS_AGRESOR (id_denuncia,id_agresor)
CREATE TABLE IF NOT EXISTS DENUNCIA_HAS_AGRESOR (
	id_denuncia INTEGER,
	id_agresor INTEGER,
	FOREIGN KEY(id_denuncia) REFERENCES DENUNCIA(id_denuncia),
	FOREIGN KEY(id_agresor) REFERENCES AGRESOR(id_agresor)
);
--insertamos agresores a las denuncias:
INSERT INTO DENUNCIA_HAS_AGRESOR (id_denuncia, id_agresor) VALUES (1,1), (2,3), (3,2), (3,3);



-- CHRIS
--
-- DENUNCIA_HAS_VICTIMA (id_denuncia,id_victima)
CREATE TABLE IF NOT EXISTS DENUNCIA_HAS_VICTIMA(
	id_denuncia INTEGER NOT NULL,
	id_victima INTEGER NOT NULL,
	FOREIGN KEY (id_denuncia) REFERENCES DENUNCIA(id_denuncia),
	FOREIGN KEY (id_victima) REFERENCES VICTIMA(id_victima)
);
--
 -- DENUNCIA_HAS_USUARIO (id_denuncia,id_usuario)
CREATE TABLE IF NOT EXISTS DENUNCIA_HAS_USUARIO(
	id_denuncia INTEGER NOT NULL,
	id_usuario INTEGER NOT NULL,
	FOREIGN KEY (id_denuncia) REFERENCES DENUNCIA(id_denuncia),
	FOREIGN KEY (id_usuario) REFERENCES USUARIO(id_usuario)
);
-- ROLES_HAS_USUARIO (id_roles_usuario,id_usuario,caducidad)
CREATE TABLE IF NOT EXISTS ROLES_HAS_USUARIO(
	id_roles_usuario INTEGER NOT NULL,
	id_usuario INTEGER NOT NULL,
	caducidad TEXT,
	FOREIGN KEY (id_roles_usuario) REFERENCES ROLES_USUARIO(id_roles_usuario),
	FOREIGN KEY (id_usuario) REFERENCES USUARIO(id_usuario)
);
-- DENUNCIA_HAS_ENTIDAD (id_denuncia,id_entidad)
CREATE TABLE IF NOT EXISTS DENUNCIA_HAS_ENTIDAD(
	id_denuncia INTEGER NOT NULL,
	id_entidad INTEGER NOT NULL,
	FOREIGN KEY (id_denuncia) REFERENCES DENUNCIA(id_denuncia),
	FOREIGN KEY (id_entidad) REFERENCES ENTIDAD_AYUDA(id_entidad)
);